<?php

function parse_tabledef($fileName) {
	$cols = array();
	$tableName = 'Table name undefined!';
	foreach(file($fileName) as $f) {
		if(substr($f,0,1)=='#') continue; // ignore comment lines starting with '#'
		if(substr($f,0,1)=='@') {
			$tableName = explode('@',trim($f))[1];
			continue;
		}
		if(substr($f,0,1)=='$') {
			$cols['$'] = '$';
			break; // I think this was supposed to let '$' indicate end of config, so stop here
		}
	$f = trim($f);
	$r = explode('"',$f);
		$name  = trim(explode(' ',  trim($r[0]))[0]);
		$type  = trim(explode($name,trim($r[0]))[1]);
		$title = (isset($r[1]))?trim($r[1]):'Undefined Field Title';
		$class = (isset($r[2]))?trim($r[2]):'';
		$cols[$name]['name'] = $name;
		$cols[$name]['type'] = $type;
		$cols[$name]['title'] = $title;
		$cols[$name]['class'] = $class;
	}
	return array($tableName,$cols);
}

function parse_season_config($fileName='season_config') {
	$events = array();
	$seasonYear = '0000';
	$seasonName = 'Undefined Season Name';
	foreach(file($fileName) as $f) {
		if(substr($f,0,1)=='#') continue; // ignore comment lines starting with '#'
		if(is_numeric(substr($f,0,4))) { // if first token is year, next is season name
			$seasonYear = explode(':',trim($f))[0]; // before the ':' is year
			$seasonName = explode(':',trim($f))[1]; // after the ':' is name
			continue;
		}
		if(substr($f,0,1)=='$') {
			$events['$'] = '$';
			break;
		}
	$f = trim($f);
	$e = explode('"',$f);
		$key  = trim(explode(' ',$e[0])[0]);
		$type = trim(explode(' ',$e[0])[1]);
		$name  = (isset($e[1]))?trim($e[1]):'Undefined Event Name';
		$class = (isset($e[2]))?trim($e[2]):'';
		$events[$key]['key']   = $key;
		$events[$key]['type']  = $type;
		$events[$key]['name']  = $name;
		$events[$key]['class'] = $class;
	}
	return array($seasonName,$seasonYear,$events);
}

function parse_event_index_config($fileName='event_index_config') {
// don't use '#' for comments so we can link to an #id
	$comment = ';';
	$eof = '$';
	$pages = array();
	$row = '';
	foreach(file($fileName) as $f) {
	// ignore comment lines starting with $comment
		if(substr($f,0,strlen($comment))==$comment)
			{continue;}
	// $eof indicates EOF as far as parsing is concerned
		if($f==$eof)
			{break;}
	// each row will begin with a [Line of Display Text]
		if(substr($f,0,1)=='[' & strpos($f,']')!==false)
		{
			$row = explode(']',substr($f,1))[0];
			$pages[$row]['text'] = $row;
			$pages[$row]['href'] = '';
			$pages[$row]['title'] = '';
			$pages[$row]['class'] = '';
			$pages[$row]['id'] = '';
			continue;
		}
	// each [Line of Display Text] will be followed by zero or more
	// properties of the form key=value
	// supported properties match /(href|title|class|id)/
		if(strpos($f,'=') !== false) {
			$pages[$row][explode('=',$f)[0]] = substr($f,strpos($f,'=')+1);
		}
	}
	return $pages;
}


?>