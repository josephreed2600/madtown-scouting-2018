<?php

$mysqli_object = true;

if(!isset($MYSQLI_CONNECTED)) {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	$server = 'localhost';
	$username = 'ya boi scouting user';
	$password = 'a password set by ya boi';
	$database_name = 'the database of ya boi';
	$db = ($mysqli_object)?
		new mysqli($server,$username,$password,$database_name):
		mysqli_connect($server, $username, $password, $database_name);	
	unset($server);
	unset($username);
	unset($password);
	unset($database_name);
$MYSQLI_CONNECTED = true;
}

if(!isset($MYSQLI_FUNCTIONS)) {
/** Create a query. */
	function dbq($query) {
		global $db, $mysqli_object;
		return ($mysqli_object)?
			$db->query($query):
			mysqli_query($db,$query);
	}
/** Access the last database error. */
	function dberr() {
		global $db, $mysqli_object;
		return ($mysqli_object)?
			$db->error:
			mysqli_error($db);
	}
/** Fetch the next row of a query as an associative array. */
	function dbfetch($query) {
		global $mysqli_object;
		return ($mysqli_object)?
			$query->fetch_assoc():
			mysqli_fetch_assoc($query);
	}
/** Fetch the next row of a query as an object. */
	function dbfetchObj($query) {
		global $mysqli_object;
		return ($mysqli_object)?
			$query->fetch_object():
			mysqli_fetch_object($query);
	}
$MYSQLI_FUNCTIONS = true;
}
?>
