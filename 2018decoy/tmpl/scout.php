<?php
include 'includes.php';

print html_top();
print html_usual(true);
print html_title('Scout');
?>
<style>
body {margin: 1em;}
fieldset {margin: 0 1px;}
* {user-select: none;}

.redTeam {
    background: #E44;
    color: black;
}

.blueTeam {
    background: #45B;
    color: white;
}
</style>

<!--script>
// adapted from http://www.emanueleferonato.com/2006/06/04/javascript-chronometerstopwatch/
var timercount = 0;
var timestart  = null;
 
function showtimer() {
	if(timercount) {
		clearTimeout(timercount);
		clockID = 0;
	}
	if(!timestart){
		timestart = new Date();
	}
	var timeend = new Date();
	var timedifference = timeend.getTime() - timestart.getTime();
	timeend.setTime(timedifference);
	var seconds_passed = timeend.getSeconds();
	$('[name=climb]').val(seconds_passed);
	timercount = setTimeout("showtimer()", 1000);
}
 
function sw_start(){
	if(!timercount){
	timestart   = new Date();
	$('[name=climb]').val(0);
	timercount  = setTimeout("showtimer()", 1000);
	}
	else{
	var timeend = new Date();
		var timedifference = timeend.getTime() - timestart.getTime();
		timeend.setTime(timedifference);
		var seconds_passed = timeend.getSeconds();
	}
}
 
function sw_stop() {
	if(timercount) {
		clearTimeout(timercount);
		timercount  = 0;
		var timeend = new Date();
		var timedifference = timeend.getTime() - timestart.getTime();
		timeend.setTime(timedifference);
		var seconds_passed = timeend.getSeconds();
		$('[name=climb]').val(seconds_passed);
	}
	timestart = null;
}
 
function sw_reset() {
	timestart = null;
	$('[name=climb]').val(0);
}
</script-->

<script>
var form = '#scoutForm';
var $request = '';
var fetchNewTeams = new Event('fetchNewTeams');
// First time initializations										//// session -> local
if(localStorage.getItem('scout'+$FQEK) === null) localStorage.setItem('scout'+$FQEK,'[]');
if(localStorage.getItem('new'+$FQEK) === null) localStorage.setItem('new'+$FQEK,'[]');


function uriToJSON($search) {
	return $search?JSON.parse('{"' + $search.replace(/&/g, '","').replace(/=/g,'":"') + '"}',
		function(key, value) { return key===""?value:decodeURIComponent(value) }):{};
}
function getThisData() {
	var formData = $(form).serialize();
	formData = uriToJSON(formData);
	$('input[type=checkbox]').each(function(idx,el){formData[$(el).attr('name')] = (el.checked?1:0)});
//	formData.climbSuccess = (~~formData.climb)?1:0;
	return formData;
}
function storeData($thisData = getThisData(), $queue = false) {
	var $key = ($queue)?'new':'scout';
	$key += $FQEK;
	var $data = localStorage.getItem($key);								//// session -> local
	$data = JSON.parse($data);
	$data.push($thisData);
	$data = JSON.stringify($data);
	localStorage.setItem($key,$data);								//// session -> local
	if(!$queue) storeData($thisData,true);
}
function sendQueuedData() {
	return $.ajax({
		url: "insertData.php",
		method: "POST",
		data: 'data='+localStorage.getItem('new'+$FQEK),					//// session -> local
		success: function(resp) {
				localStorage.setItem('new'+$FQEK,'[]');					//// session -> local
				console.log(resp);
				},
		error: console.log //function() {
//				console.log('bad stuff happened :(');
//				}
	});
}
function resendAllData() {
	localStorage.setItem('new'+$FQEK,localStorage.getItem('scout'+$FQEK));				//// session -> local
	$request = sendQueuedData();
	return $request;
}
function resetForm() {
	$('input:not(.no-reset):not(.increment)').val(function(){
		switch (this.type){
			case 'text':
				return this.defaultValue;
				break;
			case 'number':
				return $(this).attr('data-reset');
				break;
			case 'checkbox':
			case 'radio':
				this.checked = this.defaultChecked;
		}
	});
	$('textarea:not(.no-reset)').val('');
	$('.increment').each(function(){this.value = (this.value - (-1));});
	$('input[type=text].no-reset, input[type=text].increment').each(function(){
		$(this).attr('value',$(this).val());
	});
	document.getElementById('matchNumber').dispatchEvent(fetchNewTeams);
}

function increment(event) {
	var e = $(this).siblings('input');
	var v = parseInt(e.val());
	var i = parseInt($(this).attr('data-inc'));
	var m = parseInt(e.attr('min'));
	e.val((v+i<m)?m:v+i);
}

function setTeamNumberSelectColor() {
// adapted from https://stackoverflow.com/a/16345225/6627273
	$('#teamNumber').each(function() {
		$(this).addClass($(this).children(':selected')[0].classList[0]);
	}).on('change', function(ev) {
		$(this).removeClass('redTeam').removeClass('blueTeam').addClass($(this).children(':selected')[0].classList[0]);
	});
}

var scoutSettings = [];
scoutSettings.push({"name":"Bailey",  "color":"red",  "position":"left"});
scoutSettings.push({"name":"Rajan",   "color":"red",  "position":"center"});
scoutSettings.push({"name":"Johnny",  "color":"red",  "position":"center"});
scoutSettings.push({"name":"Gabriel", "color":"red",  "position":"right"});
scoutSettings.push({"name":"Alexis",  "color":"blue", "position":"right"});
scoutSettings.push({"name":"Yurvaj",  "color":"blue", "position":"center"});
scoutSettings.push({"name":"Luis",    "color":"blue", "position":"left"});

scoutSettings.push({"name":"Victor","color":"red","position":"med"});

// Backups
scoutSettings.push({"name":"Yeilen"});
scoutSettings.push({"name":"Isaac","color":"red","position":"high"});
scoutSettings.push({"name":"Aaron"});
scoutSettings.push({"name":"Julian"});
scoutSettings.push({"name":"Joseph"});
scoutSettings.push({"name":"Angel"});

function getScout(name) {return scoutSettings.filter(function(e){return e.name.toLowerCase()==name.toLowerCase();})[0];}

function updateScoutSettings(){
	var settings = scoutSettings.find(function(e){return e.name===$('#scout').val();});
	if(settings.hasOwnProperty('color') && $('select#teamNumber')[0]) {
		$('#teamNumber').children().filter(function(e){return !($(this).hasClass(settings.color+'Team'));}).remove();
		setTeamNumberSelectColor();
	}
	if(settings.hasOwnProperty('position')) {
/*		var teams = [];
		$('#teamNumber').children().each(function(){teams.push(this.value)});
		var min = teams.reduce(function(a,b){return Math.min(a,b);})
		var max = teams.reduce(function(a,b){return Math.max(a,b);})
		var mid = teams.filter(function(a){return a!=min && a!=max;})[0];/**/
		switch(settings.position) {
/*			case 'low':
				$('#teamNumber').val(min);
				break;
			case 'med':
				$('#teamNumber').val(mid);
				break;
			case 'high':
				$('#teamNumber').val(max);
				break;/**/
			case 'left':
			case 'right':
			case 'center':
				$('#position').val(settings.position);
				break;
			default: break;
		}
	}
}

function updateTeamNumbers() {
	updateScoutSettings();
	console.log('tryna update team numbers');
	var matches = JSON.parse(localStorage.getItem('schedule'+$FQEK)).filter(function(e){return e.matchNumber==$('#matchNumber').val();});
	if(matches.length!=6) {
		console.log('matches===[]');
		$('#teamNumber').replaceWith('<input id="teamNumber" name="teamNumber" type="number" placeholder="1323" value="" class="form-control input-md teamNumber">');
	} else {
		console.log("there's matches");
		$('#teamNumber').replaceWith('<select id="teamNumber" name="teamNumber" class="form-control teamNumber" required="required"></select>');
		matches.forEach(function(e){$('#teamNumber').append('<option value="'+e.teamNumber+'" data-color="'+e.color+'" class="'+e.color+'Team">'+e.teamNumber+'</option>');});
		$('#teamNumber')[0].dispatchEvent(new Event('change'));
		setTeamNumberSelectColor();
	}
	
	console.log(matches);
}


$(function(){
	for(s of scoutSettings) {
		$('#scout').append('<option value="'+s.name+'">'+s.name+'</option>');
	}
	$.ajax({url:"/api/v2018/refreshSchedule.php?FQEK="+$FQEK});
	$.ajax({
		url: "/api/v2018/fetchSchedule.php?FQEK="+$FQEK,									//// 2018
		method: "GET",
		success: function(response) {
				localStorage.setItem('schedule'+$FQEK,response);					//// session -> local
				},
		error: function() {
				console.log('bad stuff happened in schedule-fetching :(');
				}
	});
	
	var scoutField = $('#scout')[0];
	scoutField.onchange = updateTeamNumbers;
	var matchNumberField = $('#matchNumber')[0];
	matchNumberField.addEventListener('fetchNewTeams',updateTeamNumbers);
	matchNumberField.onchange = updateTeamNumbers;
	updateTeamNumbers();
	
	
	$(form).on('submit', function(event){
		event.preventDefault();
		$thisData = getThisData();
		storeData($thisData);
		$request = sendQueuedData();
		resetForm();
	});
	$('#resend').on('click', function(event){
		event.preventDefault();
		resendAllData();
	});
	$('span.input-group-addon').on('click',increment);
	
	onoffline = function(){onbeforeunload = function(){return true;};};
	ononline = function(){onbeforeunload = null;};
	
//	$('input[name=sw_start]').on('click',sw_start);
//	$('input[name=sw_stop]').on('click',sw_stop);
//	$('input[name=sw_reset]').on('click',sw_reset);
});
</script>

<?=html_mid()?>

<form id="scoutForm" class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend><?=$EventName.' '.$Year?></legend>

<!-- Scout Name: Text input -->
<div class="form-group">
  <label class="col-md-4 control-label" for="scout">Scout</label>  
  <div class="col-md-2">
    <select id="scout" name="scout" class="form-control no-reset" required="">
    </select>
  </div>
</div>
<hr>

<!-- Match Number: Text input -->
<div class="form-group">
  <label class="col-md-4 control-label" for="matchNumber">Match</label>  
  <div class="col-md-2">
    <input id="matchNumber" name="matchNumber" type="number" placeholder="69" value="1" min="1" max="128" class="form-control input-md increment" required="">
  </div>
</div>


<!-- Team Number: Select Basic and Text Input -->
<div class="form-group">
  <label class="col-md-4 control-label" for="teamNumber">Team</label>
  <div class="col-md-2">
    <!--select id="teamNumber" name="teamNumber" class="form-control teamNumber">
      <option value="1323">1323</option>
      <option value="1678">1678</option>
      <option value="254">254</option>
    </select-->
    <input id="teamNumber" name="teamNumber" type="number" placeholder="1323" value="" class="form-control input-md teamNumber" required="">
  </div>
</div>


<!-- Position: Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="position">Position</label>
  <div class="col-md-2">
    <select id="position" name="position" class="form-control" class="no-reset">
      <option value="left">Left</option>
      <option value="center">Center</option>
      <option value="right">Right</option>
      <option value="noshow">No Show</option>
    </select>
  <span class="help-block">From your alliance's perspective<br>If no show, pick `No Show'</span>
  </div>
</div>

<hr>

<!-- Auto Baseline: Multiple Checkboxes (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="autoBaseline">Auto Baseline</label>
  <div class="col-md-4">
    <label class="checkbox-inline" for="autoBaseline">
      <input type="checkbox" name="autoBaseline" id="autoBaseline" value="1">
      Ja
    </label>
  <span class="help-block">Any part of the robot crosses the line.<br>
				Robot may go back after crossing line.</span>
  </div>
</div>

<!-- Auto Low: Appended Input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="low">Auto Low Boxes</label>
  <div class="col-md-2">
    <div class="input-group">
      <span class="input-group-addon" data-for="autoLow" data-inc="-1">-</span>
      <input id="autoLow" name="autoLow" class="form-control" placeholder="0" min="0" value="0" data-reset="0" type="number" required="">
      <span class="input-group-addon" data-for="autoLow" data-inc="1">+</span>
    </div>
  </div>
</div>

<!-- Auto High: Prepended text-->
<div class="form-group">
  <label class="col-md-4 control-label" for="autoHigh">Auto High Boxes</label>
  <div class="col-md-2">
    <div class="input-group">
      <span class="input-group-addon" data-for="autoHigh" data-inc="-1">-</span>
      <input id="autoHigh" name="autoHigh" class="form-control" placeholder="0" min="0" value="0" data-reset="0" type="number" required="">
      <span class="input-group-addon" data-for="autoHigh" data-inc="1">+</span>
    </div>
  </div>
</div>

<hr>

<!-- Low: Appended Input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="low">Low Boxes</label>
  <div class="col-md-2">
    <div class="input-group">
      <span class="input-group-addon" data-for="low" data-inc="-1">-</span>
      <input id="low" name="low" class="form-control" placeholder="0" min="0" value="0" data-reset="0" type="number" required="">
      <span class="input-group-addon" data-for="low" data-inc="1">+</span>
    </div>
  </div>
</div>

<!-- High: Prepended text-->
<div class="form-group">
  <label class="col-md-4 control-label" for="high">High Boxes</label>
  <div class="col-md-2">
    <div class="input-group">
      <span class="input-group-addon" data-for="high" data-inc="-1">-</span>
      <input id="high" name="high" class="form-control" placeholder="0" min="0" value="0" data-reset="0" type="number" required="">
      <span class="input-group-addon" data-for="high" data-inc="1">+</span>
    </div>
  </div>
</div>

<!-- Vault: Appended Input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="vault">Vault</label>
  <div class="col-md-2">
    <div class="input-group">
      <span class="input-group-addon" data-for="vault" data-inc="-1">-</span>
      <input id="vault" name="vault" class="form-control" placeholder="0" min="0" value="0" data-reset="0" type="number" required="">
      <span class="input-group-addon" data-for="vault" data-inc="1">+</span>
    </div>
  </div>
</div>

<!-- Climb: Input-->
<!--div class="form-group">
  <label class="col-md-4 control-label" for="climb">Climb</label>
  <div class="col-md-2">
    <div class="input-group">
      <!--input id="climb" name="climb" class="form-control" placeholder="0" min="0" max="15" value="0" type="number" required=""-->

<!-- Climb Success: Multiple Checkboxes (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="climbSuccess">Climb</label>
  <div class="col-md-4">
    <label class="checkbox-inline" for="climbSuccess">
      <input type="checkbox" name="climbSuccess" id="climbSuccess" value="1">
      Yeet
    </label>
  <span class="help-block">The team does the hanging or climbs aboard someone who hangs.</span>
  </div>
</div>


<!-- TBH: Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="tbh">TBH</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="tbh" name="tbh" placeholder="ur mum drives better than this kid"></textarea>
  <span class="help-block">Died, failed to appear, solid bot but bad driving, bumpers are the epitome of "hood"&mdash;anything notable.<br>
			"Sucks" doesn't really help tho, bc it doesn't say anything.<br>
			Explain why they sucked. Write notes about the match that was just played, but remember that you're writing them to help <em>predict</em> how they'll do in the future.</span>
  </div>
</div>

<!-- Submit: Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary">Submit</button>
  </div>
</div>

<!-- Resend: Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="resend"></label>
  <div class="col-md-4">
    <button id="resend" name="resend" class="btn btn-danger">Resend</button>
  </div>
</div>

</fieldset>
</form>

<?=html_bottom()?>