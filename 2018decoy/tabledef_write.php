<?php
include 'includes.php';
if(ispost('config') && ispost('table')) {
$EventKey = (ispost('eventKey'))?post('eventKey'):'.';
$Dir = $EventKey.'/';
$my_file = $Dir.'tabledef_'.post('table');
	echo 'Writing to file '.$my_file;
$handle = fopen($my_file, 'w') or die('Cannot open file: '.$my_file);
$data = post('config');
fwrite($handle, $data);
fclose($handle);

/*

It appears that the basic method for modifying a file on a line-by-line basis is this:

$lines = file($fileName);
foreach ($lines as $lineNo => $lineStr) {
	// do stuff with each line
}
file_put_contents($fileName, implode(PHP_EOL, $lines));

*/

// from eventConstructor.php
// see Search Function
$fileName = $my_file;
	if (file_exists($fileName)) {
		$lines = file($fileName);
		array_walk_recursive($lines, function(&$line) {$line = trim($line);});
		foreach ($lines as $lineNo => $lineStr) {
			if (substr($lineStr,0,1)=='@') {
				$lines[$lineNo] = $lineStr . $FQEK;
				break;
			}
		}
		file_put_contents($EventKey.'/'.$fileName, implode(PHP_EOL, $lines));
	}

} else echo 'HLEP in tabledef_write.php';
?>