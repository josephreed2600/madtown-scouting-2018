<?php
include 'includes.php';
?>

<?php
$currentEventName = $EventName;
$currentEventKey = $EventKey;
$currentFQEK = $FQEK;
$currentSeasonSegment = $SeasonSegment;

?>


<?=html_top()?>
<?=html_usual()?>

<?php

$events = parseJSONfile('season_config');

$season = '';
$offseason = '';
foreach($events as $e) {
	$e = $e['EventKey'];
	if(is_readable($e.'/values.php')) {
		include_once $e.'/values.php';
		$li = '<li><a href="'.$e.'"><span class="arrow right empty"></span>'.$EventName.'</a></li>'.PHP_EOL;
		if($SeasonSegment == "season") {$season = $li.$season;} // use .= instead to have chronological ascending (first to last)
		if($SeasonSegment == "offseason") {$offseason = $li.$offseason;}
	} // else fail silently
}

?>

<?=html_title($Year.' '.$SeasonName)?>
<script>
$(function() {
	$('li.season>a').click(function(){
		$('li.season>ul').slideToggle();
		$('li.season>a>.arrow:not(.empty)').toggleClass('open');
		});
	$('li.offseason>a').click(function(){
		$('li.offseason>ul').slideToggle();
		$('li.offseason>a>.arrow:not(.empty)').toggleClass('open');
		});

});
</script>
<?=css('arrows')?>
<style>
.arrow {
	border: 0.25rem solid transparent;
	display: inline;
	font-size: 0;
	float: left;
	margin-top: 0.425rem;
}

.arrow.right.empty {
    border: #0077C8 solid 0.125rem;
    border-bottom: 0.05rem solid transparent;
    border-left: 0.05rem solid transparent;
    transform: translateX(-0.5rem) rotate(45deg);
    width: 0.3rem;
    height: 0.3rem;
}

.arrow:not(.empty) {
	transition-duration: 350ms;
}

.arrow.right:not(.empty) {
    margin-left: -0.25rem;
    margin-right: 0.25rem;
}

.arrow.right.open {
	transform: rotate(90deg) translate(2px, 3px);
}
</style>
<style>
/* uncomment to restore one-open behavior */
li/*.<?=($currentSeasonSegment=='season')?'offseason':'season'?>/**/ > ul {
	display: none;
}
</style>
</head>
<body>

<nav>
<ul>
	<li class="heading wide-screen">
		<span class="heading logo season-name wide-screen"><?=vowelify(strtoupper($SeasonName))?></span>
	</li>
	<li class="heading narrow-screen">
		<span class="heading logo season-year narrow-screen"><?=$Year?></span>
	</li>
	<?php // Remove '" data-open="' in span.arrow.right below to restore default one-open appearance ?>
	<li class="season"><a href="#"><span class="arrow right" data-open="<?=($SeasonSegment=='season')?' open':''?>"></span>Season</a>
		<ul>
			<?=$season?>
		</ul>
	</li>
	<li class="offseason"><a href="#"><span class="arrow right" data-open="<?=($SeasonSegment=='offseason')?' open':''?>"></span>Offseason</a>
		<ul>
			<?=$offseason?>
		</ul>
	</li>

	<li class="live"><a href="input.php" style="background: #024800;"><span class="arrow right empty"></span>LIVE MATCH: Input</a></li>
	<li class="live"><a href="output.php" style="background: #920000;"><span class="arrow right empty"></span>LIVE MATCH: View</a></li>

	<li class="scout"><a href="<?=$currentEventKey?>"><span class="arrow right empty"></span>Scout: <?=$currentEventName?></a></li>
	<li class="beta"><a href="<?=$TemplateEventKey?>"><span class="arrow right empty"></span>Beta: <?=strtoupper($currentEventKey)?></a></li>
	<li class="new"><a href="createNewEvent.php"><span class="arrow right empty"></span>Create New Event</a></li>
	<li class="todo"><a href="todo.html"><span class="arrow right empty"></span>TODO</a></li>
</ul>
</nav>

</body>
</html>