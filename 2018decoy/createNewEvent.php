<?php
include 'includes.php';
?>

<!--
-- Here's the game plan:
--  1. Event Name
--  2. Event Key
--  3. Data Table Def
------------------------ Automation takes over here (see eventConstructor.php)
--  4. Update ./values.php to reflect current event
--  5. recurse_copy($eventKey,$TemplateEventKey);
--  6. Change $eventKey/values.php
--  7. Copy ./tabledef_(data|pictures) -> $eventKey/tabledef_\1
-->

<?=html_top()?>
<?=html_usual(true)?>

<title>Create New Event</title>

<style type="text/css">
html {max-width: 98%; padding: 1em;}
.col-md-4.control-label {max-width: 11em;}
.col-md-4:not(.control-label) {max-width: 20em;}
.col-md-2:not(.control-label) {max-width: 10em;}

iframe {
	width: 100%;
	border: none;
}
</style>

<script>

	var $tablesToConfig = ['data','pit'];

	function refreshEventValues() {
		if($('#EventName').val()) $EventName = $('#EventName').val();
		if($('#EventKey' ).val()) $EventKey  = $('#EventKey' ).val();
		$FQEK = $Year + $EventKey;
		$tableName = "data"+$FQEK;
	}
	
	function genTableConfig($tableType = "data") {
		refreshEventValues();
//		$tableType = "data";
		$tableName = $tableType+$FQEK;
		return {"tableName":$tableName,"tableType":$tableType,"Year":$Year,"EventKey":$EventKey,"FQEK":$FQEK,"EventName":$EventName,"SeasonSegment":"","columns":tableToJSON()};
	}
	
	function genSQLCreateQuery($tableType = "data") {
		let $c = genTableConfig($tableType);
		let $sep = '';
		$q = 'CREATE TABLE '+$c.tableName+' (';
		$c.columns.forEach(function($a){
			$q += $sep + ' ' + $a.field + ' ' + $a.type + ' ' + $a.extra;
			$sep = ','
		});
		$q += ')';
		return $q;
	}
	
	function genSQLDropQuery($tableType = "data") {
		let $c = genTableConfig($tableType);
		$q = 'DROP TABLE IF EXISTS '+$c.tableName;
		return $q;
	}
	
	function query($query) {
		return "query="+encodeURIComponent($query);
	}
	
	function doSQL($query) {
		console.log("Call to doSQL(): "+$query);
		return $.ajax({url:"/sql.php",method:"POST",data:query($query)});
	}	
	
	//$D = doSQL("query="+encodeURIComponent(genSQLDropQuery()));
	//$C = doSQL("query="+encodeURIComponent(genSQLCreateQuery()));
	
	function createSQLTable() {
		refreshEventValues();
		for(let $tbl of $tablesToConfig) {
			$.when(doSQL(genSQLDropQuery($tbl))).done(function() {
				doSQL(genSQLCreateQuery($tbl));
			});
		}
	}
	
	$(function(){
//		for(let $tbl of $tablesToConfig) {
//			$('#configTable').append('<div id="configTable-'+$tbl+'"></div>');
//			$('#configTable-'+$tbl).load('/api/v'+$Year+'/configTable.php?included&Year='+$Year+'&table='+$tbl);
//			$('#configTable').append('<div class="configTable" id="configTable-'+$tbl+'"><iframe src="/api/v'+$Year+'/configTable.php?Year='+$Year+'&table='+$tbl+'"></iframe></div>');
//			$('#configTable-'+$tbl).load('/api/v'+$Year+'/configTable.php?included&Year='+$Year+'&table='+$tbl);
//		}
		$('#configTable').load('/api/v'+$Year+'/configTable.php?included&Year='+$Year+'&table=data');
		$('.configTable').css({});
		$('#new_event_form').on('submit', function(event) {
		// courtesy of https://stackoverflow.com/a/16416162/6627273
		// ew
			event.preventDefault();
		//	createSQLTable();
			var form = this, $form = $(form);
			refreshEventValues();
		//	for(let $tbl of $tablesToConfig) {
			$.when($.ajax({
				url: "/sql.php",
				method: "POST",
				data: query(genSQLDropQuery())
				})).done(function(){
					$.ajax({
						url: "/sql.php",
						method: "POST",
						data: query(genSQLCreateQuery()),
						success: function(){
								$form.off('submit').submit();
							}
					});
				});
		//	}
		});
	});
</script>

<?=html_mid();?>
<div id="new_event_form_container">


<form id="new_event_form" class="form-horizontal" method="post" action="eventConstructor.php">
<fieldset>

<!-- Form Name -->
<legend>Create New Event</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="EventName">Event Name</label>  
  <div class="col-md-4">
  <input id="EventName" name="EventName" type="text" placeholder="MadTown Throwdown" class="form-control input-md" required="" onkeypress="$EventName=this.value;">
  <span class="help-block">Long name of the event</span>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="EventKey">Event Key</label>  
  <div class="col-md-2">
  <input id="EventKey" name="EventKey" type="text" placeholder="mttd" class="form-control input-md" required="">
  <span class="help-block">Short code of the event</span>  
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="SeasonSegment">Season Segment</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="duringseason">
      <input type="radio" name="SeasonSegment" id="duringseason" value="season" checked="checked">Season</label> 
    <label class="radio-inline" for="offseason">
      <input type="radio" name="SeasonSegment" id="offseason" value="offseason">Offseason</label>
  </div>
</div>

</fieldset>
</form>



</div>
<div id="configTable"><!--Table configuration editor failed to load :(--></div>
<hr/>
<input form="new_event_form" name="submit" type="submit" value="Create Event">
<?=html_bottom();?>