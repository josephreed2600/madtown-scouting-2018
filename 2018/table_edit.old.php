<?php
include 'includes.php';
$tableName = get('table');

//<input id="name" name="name" type="text" placeholder="teamNumber" class="form-control input-md" required="">
function input($name, $type, $placeholder,$required,$id,$class='form-control input-md'){
	$element = '<input id="'.$id.'" name="'.$name.'" placeholder="'.$placeholder.'" class="'.$class.'"';
//	$element .= ($required==true)?' required>':'>';
	$element .= '>';
	return $element;
}

?>

<?=html_top()?>
<?=css('bootstrap')?>
<?=js('jquery.min')?>
<?=js('bootstrap')?>

<title>Table Editor</title>

<!-- https://stackoverflow.com/questions/2145012/adding-rows-dynamically-with-jquery -->
<script type="text/javascript">
var table='#formtable';
var addRow='#addRow';
    $(document).ready(function() {
        $(addRow).click(function() {
//          $(table+'tbody>tr:last').clone(true).insertAfter(table+'tbody>tr:last');
		$(table).toggle();
          return false;
        });
    });
</script>

<script>
function post($url,$data) {
	var http = new XMLHttpRequest();
//	var url = "/1323/editdata.php";
	var url = $url;
//	var params = "query=show tables";
	var params = $data;
	result = '';
	http.open("POST", url, true);
	
	//Send the proper header information along with the request
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http.onreadystatechange = function() {
		if(http.readyState == 4 && http.status == 200) {
		return http.responseText;
	    }
	}
	http.send(params);
	return http.onreadystatechange();
}
</script>
<script>
function deleteRow(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("myTable").deleteRow(i);
}
</script>

<?=html_mid()?>

<form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Editing Table: <?=$tableName?></legend>
<table id="formtable">
<tbody>
<tr>
<td><?=input('name','text','teamNumber',0,'name')?></td>
<td><?=input('type','text','tinyint',0,'type')?></td>
<td><?=input('title','text','Team Number',0,'title')?></td>
<td><button form="" id="remove" name="remove" class="btn btn-danger" onclick="deleteRow(this);">-</button></td>
</tr>
</tbody>
</table>
<button id="addRow">
</fieldset>
</form>

<?=html_bottom()?>