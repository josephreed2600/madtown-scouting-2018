<?php
include 'includes.php';

$tbl = parseJSONfile('tabledef_data');
$cols 		= $tbl->columns;
$tableName 	= $tbl->tableName;
$tableType 	= $tbl->tableType;
$EventName 	= $tbl->EventName;
$EventKey 	= $tbl->EventKey;
$FQEK 		= $tbl->FQEK;
$SeasonSegment 	= $tbl->SeasonSegment;


echo '<pre>';


// This page requires a JSON array POSTed as 'data'
// If no data, use empty array (valid JSON)
$data = (ispost('data'))?$_POST['data']:'[]';
if($data == '[]') {
	echo __FILE__.' received either an empty set or no data';
//	exit();
}
$data = json_decode($data);

if($data===false) {
	echo __FILE__.' failed to decode JSON: '.json_last_error_msg();
	exit();
}

// Logic: Delete * from table where conditions; insert into table

// Delete
$queryDelete = 'DELETE FROM '.$tableName.' WHERE teamNumber=? AND matchNumber=?';
echo PHP_EOL.$queryDelete.PHP_EOL;
$delete = $db->stmt_init();
if($delete->prepare($queryDelete)) { // preparation returns its success
	$delete->bind_param("ii",$teamNumber,$matchNumber);
	foreach($data as $r) {
		$teamNumber = $r->teamNumber;
		$matchNumber = $r->matchNumber;
		echo 'Deleting from '.$tableName.' where teamNumber='.$teamNumber.' and matchNumber='.$matchNumber.PHP_EOL;
		$delete->execute();
	}
	$delete->close();
} else {
	echo __FILE__.': Failure to prepare DELETE statement: '.$queryDelete.PHP_EOL;
	echo $delete->error.PHP_EOL;
	exit();
}

// Insert
$queryInsert = 'INSERT INTO '.$tableName.' (';
$queryEnd = ') VALUES (';
$sep = '';
$basetypes = '';
$params = ''; // contains the parameters for bind_params
$vars = ''; // contains the assignments from $r
$long_data = ''; // contains $insert->send_long_data() calls
$index = 0; // contains the index for long_data calls
foreach($cols as $c) {
	if(strpos($c->extra,'AUTO_INCREMENT')!==false) continue; // FIXME: check param order and return values
	$queryInsert .= $sep.$c->field;
	$queryEnd .= $sep.'?';
	$basetypes .= $c->basetype;
	$params .= $sep.'$'.$c->field;
	$vars .= '$'.$c->field.' = $r->'.$c->field.';'.PHP_EOL;
	if($c->basetype == 'b') {
		$long_data .= '$insert->send_long_data('.$index.', $'.$c->field.');'.PHP_EOL;
	}
	$index += 1;
	$sep = ', ';
}
$queryInsert .= $queryEnd.')';

// we gonna eval dis lool
$params = 'return $insert->bind_param($basetypes, '.$params.');';

echo PHP_EOL.$queryInsert.PHP_EOL;
echo PHP_EOL.$params.PHP_EOL;
echo PHP_EOL.$vars.PHP_EOL;

$insert = $db->stmt_init();
if($insert->prepare($queryInsert)) {
	eval($params); // returns boolean representing success, maybe
	foreach($data as $r) {
		echo 'Inserting match '.$matchNumber.', team '.$teamNumber.PHP_EOL;
		eval($vars);
		eval($long_data);
		$insert->execute();
	}
	$insert->close();
} else {
	echo __FILE__.': Failure to prepare INSERT statement: '.$queryInsert.PHP_EOL;
	echo $insert->error.PHP_EOL;
	exit();
}

echo '</pre>';