<?php

include 'includes.php';

$api_v = $Year;

$tbl = parseJSONfile('tabledef_data');
$tableName = $tbl->tableName;
$cols = $tbl->columns;

?>

<?=html_top()?>
<?=html_usual()?>
<?=js('jquery.tablesorter')?>

<?=css('table.data')?>
<?=css('table.data.thead')?>
<?=css('table.data.tbody')?>
<?=css('lightbox')?>

<?=html_title($EventName.' '.$Year)?>

<script>
function fetchData() {
	return $.ajax({url: "/api/v<?=$api_v?>/fetchData.php?FQEK="+$FQEK});
}

function fetchHead() {
	return $.ajax({url: "/api/v<?=$api_v?>/fetchTableHead.php?FQEK="+$FQEK, dataType: "json"});
}

function fixStyles() {
	$('.nope').remove();
	$('.wide').removeClass('tilt');
	makeBoolsNice();
	removeNulls();
}

// FIXME: Rename?
function doStuffToTheTable() {
	setTimeout(function() {
		fixStyles();
// see https://mottie.github.io/tablesorter/docs/example-option-sort-list.html
		$('table.data').tablesorter({
			sortList: [[0,0],[2,0],[1,0]] // initial sort col 0 asc, col 2 asc, col 1 asc
		});
		$.tablesorter.updateCache($('table.data')[0].config,function(table){});
	}, 1000);
}

function updateData($data) {
	$data = $data.trim().replace('\n','<br>').replace('\r','');
	$data = JSON.parse($data);
	localStorage.setItem($FQEK,JSON.stringify($data));
	$("table.data > tbody > tr").remove();
//	for(let $r of $data) {
// do this dynamically
/*		$t = '<tr class="data t'+$r.teamNumber+' m'+$r.matchNumber+'">';
		$t += '<td class="matchNumber m'+$r.matchNumber+' '+$r.teamColor+'">'+$r.matchNumber+'</td>';
		$t += '<td class="teamNumber t'+$r.teamNumber+'">'+$r.teamNumber+'</td>';
		$t += '<td class="robotPosition peg'+$r.robotPosition+'">'+$r.robotPosition+'</td>';
		$t += '<td class="autoGearSuccess bool'+$r.autoGearSuccess+'">'+$r.autoGearSuccess+'</td>';
		$t += '<td class="autoHighScored v'+$r.autoHighScored+'">'+$r.autoHighScored+'</td>';
		$t += '<td class="gearsHung v'+$r.gearsHung+'">'+$r.gearsHung+'</td>';
		$t += '<td class="climbTime v'+$r.climbTime+'">'+$r.climbTime+'</td>';
		$t += '<td class="climbSuccess bool'+$r.climbSuccess+'">'+$r.climbSuccess+'</td>';
		$t += '<td class="tbh wide">'+$r.tbh+'</td>';
		$t += '</tr>';
		$("table.data").append($t);
	}
/**/
	$thead = JSON.parse(localStorage.getItem($FQEK+'.thead'));
	$cols = $thead.columns;
	for(let $r of $data) {
		$tr = '<tr class="data t'+$r.teamNumber+' m'+$r.matchNumber+' '+$r.teamColor+'">';
		for(let $col of $cols) {
			if($col.class !== '') $col.class = ' '+$col.class;
			$tr +=	'<td class="'+$col.field+' '+$col.class+'" data-value="'+$r[$col.field]+'">'+$r[$col.field]+'</td>';
		}
		$tr += '</tr>';
		$('table.data').append($tr);
	}
	doStuffToTheTable();
}

function refreshData() {$.when(fetchData()).done(updateData);}

function updateHead($cols) {
	localStorage.setItem($FQEK+".thead",JSON.stringify($cols));
	$("table.data > thead > tr").remove();
	$cols = $cols.columns;
	$t = '<tr class="data headrow">';
	for(let $c of $cols) {
		$classes = 'colheading '+$c.field;
		if("class" in $c) {
			$classes += ' '+$c.class;
		}
		$t += '<th   class="'+$classes+'">';
		$t += '<div  class="'+$classes+' tilt">';
		$t += '<span class="'+$classes+'">';
		$t += $c.name+'</span></div></th>';
	}
	$t += '</tr>';
	$("table.data>thead").append($t);
	fixStyles();
}

function refreshTable() {
	$.when(fetchHead()).done(updateHead);
	$.when(fetchData()).done(updateData);
}

$(function() {
	refreshTable();
	onkeypress = function(e) {
		switch(e.key) {
			case 'r':
			case 'R':
				refreshData();
				break;
			default: break;
		}
	}
	
	onoffline = function(){onbeforeunload = function(){return true;};};
	ononline = function(){onbeforeunload = null;};
});
</script>

<?=html_mid()?>

<table class="data <?=$EventKey?>">
<thead>
</thead>
<tbody>
</tbody>

</table>

<?=html_bottom()?>