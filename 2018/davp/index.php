<?php
include 'includes.php';

// TODO: Move to util
function if_attr($obj, $attr) {
	$s = '';
	if(is_array($attr)) {
		foreach($attr as $a) {
			$s .= if_attr($obj, $a);
		}
	} else {
		if(is_array ($obj)) $s = (isset($obj  [$attr]))?' '.$attr.'="'.$obj  [$attr].'"':'';
		if(is_object($obj)) $s = (isset($obj->{$attr}))?' '.$attr.'="'.$obj->{$attr}.'"':'';
	}
	return $s;
}

print html_top();
print html_usual();
print html_title($EventName);
print html_mid();

$nav = parseJSONfile('event_index_config');

// TODO: Include support for nested lists
/*
{"rows":[
	{...},
	{
		"text":"foo",
		...,
		"rows":[...]
	}
]}
*/

print '<nav><ul>';
foreach($nav->rows as $item)
{
	// Evaluate expressions first
	foreach($item as $key => $value) {
		if(strpos($value,'$')!==false) { // evaluate if '$' is present in string
			$item->{$key} = eval('return '.$value.';');
		}
	}

	$li = '<li'.if_attr($item,array('class','title')).'>'.PHP_EOL;
	if(isset($item->href)) {
		$li .= '<a';
		$closer = '</a>';
	} else {
		$li .= '<span';
		$closer = '</span>';
	}
	$li .= if_attr($item,array('href','class','title')).'>';
	$li .= $item->text.$closer.PHP_EOL.'</li>'.PHP_EOL;
	print $li;
}
print '</ul></nav>';

print html_bottom();

?>