<?php
if(!(isset($ROOT_DIR) || isset($SEASON_DIR) || isset($EVENT_DIR))) {
	$ROOT_DIR = '.';
	while(!file_exists($ROOT_DIR.'/root')) {
		$ROOT_DIR .= '/..';
	//	if(file_exists($ROOT_DIR.'/includes.php')) include $ROOT_DIR.'/includes.php';
	}
	if(isset($ROOT_DIR) && strlen($ROOT_DIR)>3) $SEASON_DIR = substr($ROOT_DIR,0,-3);
	if(isset($SEASON_DIR) && strlen($SEASON_DIR)>3) $EVENT_DIR = substr($SEASON_DIR,0,-3);
	
	if(isset($ROOT_DIR) && $ROOT_DIR!='.' && file_exists($ROOT_DIR.'/includes.php')) {
		$ROOT_INCLUDE_FILE = $ROOT_DIR.'/includes.php';
		include $ROOT_INCLUDE_FILE;
	}
	if(isset($SEASON_DIR)) {
		if(file_exists($SEASON_DIR.'/includes.php' && $SEASON_DIR!='.')) {
			$SEASON_INCLUDE_FILE = $SEASON_DIR.'/includes.php';
			include $SEASON_INCLUDE_FILE;
		}
		if(file_exists($SEASON_DIR.'/util.php')) {
			$SEASON_UTIL_FILE = $SEASON_DIR.'/util.php';
			include $SEASON_UTIL_FILE;
		}
		if(file_exists($SEASON_DIR.'/values.php')) {
			$SEASON_VALUES_FILE = $SEASON_DIR.'/values.php';
			include $SEASON_VALUES_FILE;
		}
	}
	if(isset($EVENT_DIR)) {
		if(file_exists($EVENT_DIR.'/includes.php' && $EVENT_DIR!='.')) {
			$EVENT_INCLUDE_FILE = $EVENT_DIR.'/includes.php';
			include $EVENT_INCLUDE_FILE;
		}
		if(file_exists($EVENT_DIR.'/util.php')) {
			$EVENT_UTIL_FILE = $EVENT_DIR.'/UTIL.php';
			include $EVENT_UTIL_FILE;
		}
		if(file_exists($EVENT_DIR.'/values.php')) {
			$EVENT_VALUES_FILE = $EVENT_DIR.'/values.php';
			include $EVENT_VALUES_FILE;
		}
	}
}

if(	isset($EVENT_DIR) &&
	isset($SEASON_INCLUDED) &&
	!isset($EVENT_INCLUDED)) {
		$EVENT_INCLUDED = true;
} else if(
	isset($SEASON_DIR) &&
	!isset($SEASON_INCLUDED)) {
		$SEASON_INCLUDED = true;
}

?>