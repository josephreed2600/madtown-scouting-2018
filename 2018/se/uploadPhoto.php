<?php include 'includes.php'; ?>
<?=html_top().html_usual()?>

<script>
$(function(){
	$('form').on('submit',function(e) {
		e.preventDefault();
		$('#teamNumber')[0].defaultValue = $('#teamNumber').val();
		var formData = new FormData();
		
		formData.append("teamNumber",$('#teamNumber').val());
		var i = 0;
		while($('[type=file]')[0].files.hasOwnProperty(i))
			formData.append("photo[]",$('[type=file]')[0].files[i++]);
		
		var request = new XMLHttpRequest();
		request.open("POST","/api/v2018/postPhoto.php");
		request.send(formData);
		
		$('form')[0].reset();
	});
});
</script>

<?=html_mid()?>
<form id="picupload">
<input type="number" id="teamNumber" name="teamNumber" min="0" max="9999" placeholder="Team Number">
<label class="file-styler">
	<input type="file" id="photo" name="photo[]" accept="image/*;capture=camera" multiple>
</label>
<input type="submit" value="Yeet!">
</form>
<a id="return" href=".">Return to Directory</a>
<?=html_bottom()?>