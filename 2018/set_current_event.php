<?php
include 'includes.php';
echo 'included stuff!'.$nl;

if(isget('EventName') && isget('EventKey') && isget('SeasonSegment')) {
echo 'Got here'.$nl;
	$EventName = urldecode(get('EventName'));
	$EventKey = get('EventKey');
	$SeasonSegment = get('SeasonSegment');

echo '<hr/>';
echo 'set_current_event.php:'.$nl;
echo '$EventName = '.$EventName.$nl;
echo '$EventKey = '.$EventKey.$nl;
echo '$SeasonSegment = '.$SeasonSegment.$nl;
echo '<hr/>';


// Set current event in ./current_event.php
	$handle = fopen('current_event.php', 'w') or die('set_current_event.php: Cannot open file "current_event.php" for writing');
	$data = array(	'<?php',
			'$EventName = "'	.$EventName		.'";',
			'$EventKey = "'	.$EventKey		.'";',
			'$FQEK = $Year.$EventKey;',
			'$SeasonSegment = "'	.$SeasonSegment	.'";',
			'?>');
	foreach($data as $d) {
//		echo $d.PHP_EOL;
		fwrite($handle, $d.PHP_EOL);
	}
	fclose($handle);
	echo 'current event set'.$nl;


// Set event parameters in ./$EventKey/values.php
	$handle = fopen($EventKey.'/values.php', 'w') or die('set_current_event.php: Cannot open file "'.$EventKey.'/values.php" for writing');
	$data = array(	'<?php',
			'$EventName = "'.get('EventName').'";',
			'$EventKey = "'.get('EventKey').'";',
			'$FQEK = $Year.$EventKey;',
			'$SeasonSegment = "'.$SeasonSegment.'";',
			'?>');
	foreach($data as $d) {
//		echo $d.$nl;
		fwrite($handle, $d.PHP_EOL);
	}
	fclose($handle);
	echo 'event values set'.$nl;

} else echo 'failed to set current event: insufficient parameters';

?>