<?php
include 'includes.php';


//<input id="name" name="name" type="text" placeholder="teamNumber" class="form-control input-md" required="">
function input($name, $type, $value='', $placeholder='', $required=false, $id='', $class='form-control input-md', $other=''){
	$element = '<input id="'.$id.'" type="'.$type.'" name="'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'" class="'.$class.'"';
	if($required==true) $element .= ' required';
	$element .= ' '.$other.'>';
	return $element;
}

function option($value, $text) {return '<option value="'.$value.'">'.$text.'</option>';}

$tableType = 'data';

?>
<?php
// Make sure to include the following in implementations
/*
<?=css('bootstrap')?>
<?=js('jquery.min')?>
<?=js('bootstrap')?>
<?=js('http')?>
<style type="text/css">
#formtable, #addRow, #saveConfig {margin-left: 1em;}
#addRow, #saveConfig {margin-top: 1em;}
</style>
*/
?>
<div id="Dir" style="display: none;"><?=(isget('EventKey'))?'&eventKey='.get('EventKey'):'';?></div>
<form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Editing Table:
<!--select name="tableName">
<?=option('data','Data')?>
</select--> <?=$tableType?><?=$Year?><?=get('EventKey')?>
</legend>
<table id="formtable">
<tbody>

<?php
// From ./tabledefs.php

/*/
foreach(file('tabledef_'.$tableType) as $f) {
	if(substr($f,0,1)=='#') continue;
	if(substr($f,0,1)=='@') {
		$tableName = explode('@',trim($f))[1];
		continue;
	}
	if(substr($f,0,1)=='$') {
		$cols['$'] = '$';
		break;
	}
	$name = trim(explode(' ',trim($f))[0]);
	$type = trim(explode($name,explode('"',trim($f))[0])[1]);
	$title = trim(explode('"',trim($f))[1]);
	$cols[$name]['name'] = $name;
	$cols[$name]['type'] = $type;
	$cols[$name]['title'] = $title;
}
/*/
$Dir = (isget('EventKey'))?get('EventKey').'/':'';
$tbl = parse_tabledef($Dir.'tabledef_'.$tableType);
$tableName = $tbl[0];
$cols = $tbl[1];
/**/
foreach($cols as $n => $a) {
	echo '<tr>';
	foreach($a as $k => $v) {
		echo '<td>'.input($k,'text',$v,$v,false,$k).'</td>';
	}
	echo '<td>'.input('remove','button','-',null,false,null,'btn btn-danger','onclick="deleteRow(this)"').'</td>';
	echo '</tr>';
}
?>

<?php //	     Name	Type		Value		Placeholder	Req.	id	class			other ?>
<!--
<tr>
<td><?=input('name',	'text',		'teamNumber',	'teamNumber',	false,	'name')?></td>
<td><?=input('type',	'text',		'tinyint',	'tinyint',	false,	'type')?></td>
<td><?=input('title',	'text',		'Team Number',	'Team Number',	false,	'title')?></td>
<td><?=input('remove',	'button',	'-',		null,		false,	null,	'btn btn-danger',	'onclick="deleteRow(this)"')?></td>
</tr>
-->

</tbody>
</table>
<button id="addRow" class="btn btn-success">+</button>
<button id="saveConfig" class="btn btn-success">Save Configuration</button>
</fieldset>
</form>