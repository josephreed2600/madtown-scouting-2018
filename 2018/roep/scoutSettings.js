var scoutSettings = [];
scoutSettings.push({"name":"Alexis",       "color":"blue", "position":"right"});
scoutSettings.push({"name":"Bailey",       "color":"red",  "position":"left"});
scoutSettings.push({"name":"Lois",         "color":"blue", "position":"left"});
scoutSettings.push({"name":"Rajan",        "color":"red",  "position":"center"});
scoutSettings.push({"name":"Yeilen",       "color":"blue", "position":"center"});
scoutSettings.push({"name":"DDB",          "color":"red",  "position":"right"});

// Backups
scoutSettings.push({"name":"Grace"});
scoutSettings.push({"name":"Yurvaj",  "color":"blue", "position":"center"});
scoutSettings.push({"name":"Johnny",  "color":"red",  "position":"center"});
scoutSettings.push({"name":"Julian"});
scoutSettings.push({"name":"Angel"});
scoutSettings.push({"name":"Joseph"});