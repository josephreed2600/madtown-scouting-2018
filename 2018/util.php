<?php
if(!isset($SEASON_UTIL_INCLUDED)){

//////////////////////////////////////////////////////////////////////////

function thead($cols) {
	
	$thead = '<thead><tr class="data headrow">';
	
	foreach($cols as $k=>$v) {
		if(!isset($v['hidden'])) {
			if(isset($v['class'])) $v['name'] .= ' '.$v['class'];
			$thead .= '<th class="'			.$v['name'].	' colheading">
				<div class="'			.$v['name'].	' colheading tilt">
				<span class="'			.$v['name'].	' colheading">'
					.$v['title']	.'</span></div></th>';
		}
	}
	$thead .= '</tr></thead>';
	return $thead;
}

//////////////////////////////////////////////////////////////////////////

$SEASON_UTIL_INCLUDED = 1;
}
?>