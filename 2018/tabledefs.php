<?php
include 'includes.php';

////// Ahoy! If you're looking for the code that parses a table configuration
// into an array, look in /util.php. That's the new official version.
// See line 61 for an example of usage.

$tableType = 'data';
//echo $tableType.$FQEK.'<br/>';

$cols = array();

//$query = 'create table '.$tableType.$FQEK.' (';
/*
echo 'Get each column name:<br/>';
foreach(file('tabledef_'.$tableType) as $f) {
	$name = explode(' ',trim($f))[0];
	echo $name.' / ';
	$cols[$name] = array('name' => $name);
}

echo '<br/>Get each column declaration:<br/>';
foreach(file('tabledef_'.$tableType) as $f) {
	$name = explode(' ',trim($f))[0];
	$type = explode($name,explode('"',trim($f))[0])[1];
	echo $name.' '.$type.' / ';
	$cols[$name]['type'] = $type;
}

echo '<br/>Get each column title:<br/>';
foreach(file('tabledef_'.$tableType) as $f) {
	$name = explode(' ',trim($f))[0];
	$title = explode('"',trim($f))[1];
	echo $title.' / ';
	$cols[$name]['title'] = $title;
}

/**/
if(!isget('table') || !isget('EventKey')) {
echo '<pre><br/>Get each column configuration<br/>';
/*
foreach(file('tabledef_'.$tableType) as $f) {
	if(substr($f,0,1)=='#') continue;
	if(substr($f,0,1)=='@') {
		$tableName = explode('@',trim($f))[1];
		continue;
	}
	if(substr($f,0,1)=='$') {
		$cols['$'] = '$';
		break;
	}
	$name = trim(explode(' ',trim($f))[0]);
	$type = trim(explode($name,explode('"',trim($f))[0])[1]);
	$title = trim(explode('"',trim($f))[1]);
//	echo $name.' '.$type.' / ';
	$cols[$name]['name'] = $name;
	$cols[$name]['type'] = $type;
	$cols[$name]['title'] = $title;
}
/**/
$tbl = parse_tabledef('tabledef_'.$tableType);
$tableName = $tbl[0];
$cols = $tbl[1];

echo '<br/>';
echo '<br/>Table Description: '.$tableName.'<br/>';
foreach($cols as $c) echo $c['title'].':	'.$c['name'].'	'.$c['type'].'<br/>';

echo '<br/><br/>Table creation query:<br/>';
$query = 'CREATE TABLE '.$tableName.' (';
$last = array_keys($cols);
$last = array_pop($last);
foreach($cols as $c => $v) {
	$query .= $v['name'].' '.$v['type'];
	if($c != $last) $query .= ', ';
}
$query .= ')';
echo  $query;

echo '<br/>';
echo '<br/>';
echo 'Table config file:<br/>';
foreach($cols as $c) echo $c['name'].' '.$c['type'].' "'.$c['title'].'"<br/>';
echo '</pre>';
} else {

$EventKey = get('EventKey');
$FQEK = $Year.$EventKey;
$tableType = get('table');
$tableName = $tableType.$FQEK;
foreach(file($EventKey.'/tabledef_'.$tableType) as $f) {
	if(substr($f,0,1)=='#') continue;
	if(substr($f,0,1)=='@') {
		$tableName = explode('@',trim($f))[1];
		continue;
	}
	if(substr($f,0,1)=='$') {
		$cols['$'] = '$';
		break;
	}
	$name = trim(explode(' ',trim($f))[0]);
	$type = trim(explode($name,explode('"',trim($f))[0])[1]);
	$title = trim(explode('"',trim($f))[1]);
//	echo $name.' '.$type.' / ';
	$cols[$name]['name'] = $name;
	$cols[$name]['type'] = $type;
	$cols[$name]['title'] = $title;
}
/**/

$tbl = parse_tabledef($EventKey.'/tabledef_'.$tableType);
$tableName = $tbl[0];
$cols = $tbl[1];

$query = 'CREATE TABLE '.$tableName.' (';
$last = array_keys($cols);
$last = array_pop($last);
foreach($cols as $c => $v) {
	$query .= $v['name'].' '.$v['type'];
	if($c != $last) $query .= ', ';
}
$query .= ')';
print  $query;

}
?>
