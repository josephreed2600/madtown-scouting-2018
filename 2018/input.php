<?php
include '../includes.php';

print html_top();
print html_usual();
print css('common');
print js('jquery.touchSwipe.min');
?>
<script>
function updateColors(e) {
	var boxes = $(e.target).hasClass('switch') ? 'switch' : 'scale';
	$('td.'+boxes+':not(.swap)').each(function(){
		var color = $(this).attr('data-color');
		$(this).attr('data-color',color=='blue'?'red':'blue');
	});
	var colors = [];
	$('td[data-color]').each(function(){
		colors.push($(this).attr('data-color')[0]);
	});
	colors.length = 2;
	c = ~~(colors.pop() == 'b')+2*~~(colors.pop() == 'b')
//	console.log(c);
	data.c = c;
	sendData();
}

function incrementBox(box) {
	if(box.type == 'click') box = box.target;
	$(box).attr('data-value',~~($(box).attr('data-value'))+1);
	updateBox(box);
}
function decrementBox(box) {
	if(box.type == 'click') box = box.target;
	$(box).attr('data-value',~~($(box).attr('data-value'))-1);
	updateBox(box);
}

function updateBox(box) {
	if(box.type == 'click') box = box.target;
	$(box).text($(box).attr('data-value'));
	data.n = [];
	$('td.box').each(function(){data.n.push(~~$(this).attr('data-value'));});
	sendData();
}

var data = {
	"c":0,
	"n":[]
};

function sendData() {
	$.ajax({
		url: "write.php",
		method: "POST",
		data: {"d":JSON.stringify(data)}
	});
	console.log('sent data');
}

$(function(){
	$('td.swap').on('click',updateColors);
	$('td.box').on('click',incrementBox);
	$('td.box').each(function(){updateBox(this);});
	$.fn.swipe.defaults.allowPageScroll = "none";
	$.fn.swipe.defaults.preventDefaultEvents = "false";
	$('.box').swipe({
		swipeUp: function(e,dir,dist,dur,fingers){
			decrementBox(this[0]);
			
		},
		threshold: 20
		});/**/
});
</script>

<?=html_mid()?>

<table id="input">
<tr>
	<td id="box0" class="box switch left top" data-value="0" data-color="red"></td>
	<td id="box1" class="box scale top" data-value="0" data-color="red"></td>
	<td id="box2" class="box switch right top" data-value="0" data-color="red"></td>
</tr>
<tr>
	<td id="swapL" class="swap switch" data-value="0"></td>
	<td id="swapC" class="swap scale" data-value="0"></td>
	<td id="swapR" class="swap switch" data-value="0"></td>
</tr>
<tr>
	<td id="box3" class="box switch left bottom" data-value="0" data-color="blue"></td>
	<td id="box4" class="box scale bottom" data-value="0" data-color="blue"></td>
	<td id="box5" class="box switch right bottom" data-value="0" data-color="blue"></td>
</tr>
</table>

<?php
print html_bottom();
?>