<!--
From createNewEvent.php
-- Here's the game plan:
--  1. Event Name
--  2. Event Key
--  3. Data Table Def
------------------------ Automation takes over here (see eventConstructor.php)
--  3.1. Tables are created when user submits form
--  4. recurse_copy($TemplateEventKey,$eventKey);
--  5. Update ./current_event.php to reflect current event
--  6. Change $EventKey/values.php
--  7. Copy ./tabledef_data -> $EventKey/tabledef_data
--  8. Update ./tabledef_data to reflect actual table name
--  9. Call to refresh schedule for new event
-->

<?php
include 'includes.php';

// lifted from meta.php and changed to dump $_POST[]
$defined_vars = get_defined_vars()['_POST'];
ksort($defined_vars);
foreach($defined_vars as $key => $value) if(!($key=='password' || $key=='username' || $key=='defined_vars') && !is_array($defined_vars[$key])) print '$'.$key.' = \''.$value.'\';<br/>';

echo '<hr/>';
$EventName = post('EventName');
$EventKey = post('EventKey');
$FQEK = $Year.$EventKey;
$SeasonSegment = post('SeasonSegment');



$season_config_filename = 'season_config';
$season = parseJSONfile($season_config_filename);
$season[] = array('EventKey' => $EventKey);
$season = array_multidim_unique($season,'EventKey');
file_put_contents($season_config_filename,json_encode($season,JSON_PRETTY_PRINT));


recurse_copy($TemplateEventKey,$EventKey);


$url = 'http://'.$FQDN.'/'.$Year.'/set_current_event.php?EventName='.urlencode($EventName).'&EventKey='.$EventKey.'&SeasonSegment='.$SeasonSegment;
echo 'Request to: '.$url.$nl;
echo 'Request returned: '.curl($url).$nl;

foreach(array('data','pictures') as $t) {
	$file = 'tabledef_'.$t;
	
	// https://stackoverflow.com/a/19811764/6627273
	// Search Function
	if (is_readable($file)) {
		$json = parseJSONfile($file);
		if($json === null) echo 'Invalid JSON in '.$file;
		else {
			$json->tableName = $t.$FQEK;
			$json->EventName = $EventName;
			$json->EventKey = $EventKey;
			$json->Year = $Year;
			$json->FQEK = $FQEK;
			$json->SeasonSegment = $SeasonSegment;
			file_put_contents($file,json_encode($json,JSON_PRETTY_PRINT));
			copy($file,$EventKey.'/'.$file);
			copy($file,$TemplateEventKey.'/'.$file);
		}
	} else { echo 'Failed to read '.$file; }
	
}

echo (curl("/api/v2018/refreshSchedule.php"))?"Refreshed schedule":"Failed to refresh schedule";				///////// api v2018

?>
<script>
window.location.href = "<?=$EventKey?>";
</script>