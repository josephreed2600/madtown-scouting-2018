<?php
include 'includes.php';

$tbl = parse_tabledef('tabledef_data');
$tableName = $tbl[0];
$cols = $tbl[1];
$cols['_id']['hidden'] = 1;

$query = dbq('SELECT * FROM '.$tableName);
$json = '[';
$osep = '';
if($query) while($row = dbfetch($query)) {
	$json .= $osep.'{';
	$ksep = '';
	foreach($cols as $k => $c) {
		if(!isset($c['hidden'])) {
			$json .= $ksep.'"'.$k.'" : "'.$row[$k].'"';
			$ksep = ', ';
		}
	}
	$json .= '}';
	$osep = ', ';
}
$json .= ']';
print $json;