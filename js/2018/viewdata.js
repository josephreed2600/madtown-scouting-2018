function colVals(field) {
	let $vals = [];
	$('td.'+field).each(function(index) {
		let $v = $(this).attr('data-value');
		$v = (isNaN($v-0))?$v:$v-0;
		$vals[index] = $v;
	});
	return $vals;
}

function max($field) {
	return colVals($field).reduce(function(a,b){return Math.max(a,b);});
}

function min($field) {
	return colVals($field).reduce(function(a,b){return Math.min(a,b);});
}

function range($field) {
	return max($field) - min($field);
}


function c(x,field,direction='ascend') {
	let a = min(field);
	let b = max(field);
	let r = range(field);
	let u = 255 / r;
	let red = Math.round(255-(u*x));
	let green = Math.round(u*x);
	if(direction=='descend') {
		let temp = red, red = green, green = temp;
	}
	return 'rgb('+red+','+green+',0)';
}


function colorFields(fields) {
	for(let field of fields) {
		$('td.'+field).each(function(index){
			$(this).css({'background-color': c($(this).attr('data-value'),field)});
		});
	}
}

function makeBoolsNice() {
	$('td.bool').each(function(index){
		switch($(this).attr('data-value')) {
			case '0':
				$(this).text('N'); break;
			case '1':
				$(this).text('Y'); break;
			default:
				break;
		}
	});
}

function removeNulls() {
	$('td').each(function(index){
		if($(this).text() == '0') $(this).text('');
	});
}