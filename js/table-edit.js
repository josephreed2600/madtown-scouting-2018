var table='#formtable>tbody>tr:last';
var addRow='#addRow';
var saveConfig='#saveConfig';

function deleteRow(r) {
    var tblid = "formtable";
    var i = r.parentNode.parentNode.rowIndex;
	if(document.getElementById(tblid).rows.length > 1) document.getElementById(tblid).deleteRow(i);
}

function logRow($e) {
	var t = $e.children;
	var k = '';
	var f = '';
	var line = '';
	for(var i=0; i<t.length; ++i) {
		k = t[i].children[0];
		f = k.name;
		switch(f) {
			case 'name':
			case 'type':
				line += k.value + ' ';
				break;
			case 'title':
				line += '"'+k.value+'"';
				break;
			default:
				break;
		}
	}
	line += '\n';
	console.log(line);
	return line;
}

function refresh() {
	var config = '@data\n';
	var row = '';
	var rows = document.querySelector('tbody').children;
	for(var i=0; i<rows.length; ++i) {
		config += logRow(rows[i]);
	}
	return config;
}

    $(document).ready(function() {

	// https://stackoverflow.com/questions/2145012/adding-rows-dynamically-with-jquery
        $(addRow).click(function() {
          $(table).clone(true).insertAfter(table);
          return false;
        });

        $(saveConfig).click(function() {
          post('tabledef_write.php','table=data'+$('#Dir').html()+'&config='+refresh());
          return false;
        });

    });
