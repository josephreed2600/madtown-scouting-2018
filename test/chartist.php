<?php
$CHARTIST = false;
include '../includes.php';

?>



<!DOCTYPE html>
<html>
<head>
  <title>chartist.js</title>
  <?php css('chartist'); ?>
  <style type="text/css">
    body {
      margin: 0;
    }
    #container {
      position: absolute;
      width: 50%;
      height: 50%;
    }
  </style>
</head>
<body>
  <?php
	if(isget('usage')) {
			p('GET parameters:
	teamNumber	valid team number
	y		comma-separated list of fields to use as y-values
	</body>
</html>');
			exit;
		}
  ?>
  <div id="containerNope" class="ct-chart ct-golden-section"></div>
  <?php
  /*/
if(isset($SIGMA) && $SIGMA==false){
	js('sigma\/sigma.min');
	$graph = '';
	$lastNode = '';
	$lastX = 0;
	function InitGraph($graphName,$container){global $graph;
		$graph = $graphName;
		return 'var '.$graph.' = new sigma("'.$container.'");
		';}
	function LonelyNode($id,$label,$x,$y,$size,$color){global $graph;
		return $graph.'.graph.addNode({id: "'.$id.'", label: "'.$label.'", x: '.$x.', y: '.$y.', size: '.$size.', color: "'.$color.'"});
		';
	}
	function InitNode($id,$label,$x,$y,$size,$color){global $graph,$lastNode;
		$lastNode = $id;
		return LonelyNode($id,$label,$x,$y,$size,$color);
	}
	function Edge($from,$to){global $graph;
		return $graph.'.graph.addEdge({id: "'.$from.'_'.$to.'", source: "'.$from.'", target: "'.$to.'"});
		';
	}
	function TraceNode($id,$label,$x,$y,$size,$color,$lastNode){global $graph;
		$newNode = LonelyNode($id,$label,$x,$y,$size,$color);
		$newEdge = ($lastNode=='')?'':Edge($lastNode,$id);
		return $newNode.$newEdge;
	}
	function AppendNode($id,$label,$x,$y,$size,$color){global $graph,$lastNode;
		$newNode = LonelyNode($id,$label,$x,$y,$size,$color);
		$newEdge = ($lastNode=='')?'':Edge($lastNode,$id);
		$lastNode = $id;
		return $newNode.$newEdge;
	}
	function NextDatum($label,$y){global $graph,$lastX;
		$size = 1; $color = '#000';
		return ($lastX==0)?InitNode('x'.$lastX,$label,$lastX++,$y,$size,$color):AppendNode('x'.$lastX,$label,$lastX++,$y,$size,$color);
	}
	function Refresh(){global $graph;
		return $graph.'.refresh();
		';
	}
	$SIGMA = true;
}/**/
  ?>
	<?php
		$teamNumber = (isget('teamNumber'))?get('teamNumber'):1323;
		$yArray = (isget('y'))?explode(',',get('y')):['gearsHung'];
		$data = curl('http://gorohi.com/1323/api/matchData.php?teamNumber='.$teamNumber);
//		e($data.'<br/>');
		$data = preg_replace('/</','&lt;',$data);
		$data = preg_replace('/>/','&gt;',$data);
		$data = preg_replace('/\n/',' ',$data);
//		e($data);
		$data = json_decode(utf8_encode($data));
/*/		$matches = $data->matches;	// for use with curl...'&meta'
/*/		$matches = $data;		// for use without metadata in curl response
/**/
/*/		foreach($matches as $m){
//			e(($m->teamNumber).($m->tbh));
//		}
/**/
//		e($data.'<br/><br/>');
//		$json = json_encode($data);
//		$d = json_decode($data);
//		var_dump(json_encode($data));
//		e($json);
//		$d = json_decode($json,true);
//		e($d->season);
//		e(json_last_error());
	?>
	<?php js('chartist'); ?>
	<script>
		var options = {
		  // Options for X-Axis
		  axisX: {
		    // The offset of the labels to the chart area
		    offset: 30,
		    // Position where labels are placed. Can be set to `start` or `end` where `start` is equivalent to left or top on vertical axis and `end` is equivalent to right or bottom on horizontal axis.
		    position: 'end',
		    // Allows you to correct label positioning on this axis by positive or negative x and y offset.
		    labelOffset: {
		      x: 0,
		      y: 0
		    },
		    // If labels should be shown or not
		    showLabel: true,
		    // If the axis grid should be drawn or not
		    showGrid: false,
		    // Interpolation function that allows you to intercept the value from the axis label
		    labelInterpolationFnc: Chartist.noop,
		    // Set the axis type to be used to project values on this axis. If not defined, Chartist.StepAxis will be used for the X-Axis, where the ticks option will be set to the labels in the data and the stretch option will be set to the global fullWidth option. This type can be changed to any axis constructor available (e.g. Chartist.FixedScaleAxis), where all axis options should be present here.
		    type: undefined
		  },
		  // Options for Y-Axis
		  axisY: {
		    // The offset of the labels to the chart area
		    offset: 40,
		    // Position where labels are placed. Can be set to `start` or `end` where `start` is equivalent to left or top on vertical axis and `end` is equivalent to right or bottom on horizontal axis.
		    position: 'start',
		    // Allows you to correct label positioning on this axis by positive or negative x and y offset.
		    labelOffset: {
		      x: 0,
		      y: 0
		    },
		    // If labels should be shown or not
		    showLabel: true,
		    // If the axis grid should be drawn or not
		    showGrid: true,
		    // Interpolation function that allows you to intercept the value from the axis label
		    labelInterpolationFnc: Chartist.noop,
		    // Set the axis type to be used to project values on this axis. If not defined, Chartist.AutoScaleAxis will be used for the Y-Axis, where the high and low options will be set to the global high and low options. This type can be changed to any axis constructor available (e.g. Chartist.FixedScaleAxis), where all axis options should be present here.
		    type: undefined,
		    // This value specifies the minimum height in pixel of the scale steps
		    scaleMinSpace: 20,
		    // Use only integer values (whole numbers) for the scale steps
		    onlyInteger: true
		  },
		  // Specify a fixed width for the chart as a string (i.e. '100px' or '50%')
		  width: undefined,
		  // Specify a fixed height for the chart as a string (i.e. '100px' or '50%')
		  height: undefined,
		  // If the line should be drawn or not
		  showLine: true,
		  // If dots should be drawn or not
		  showPoint: true,
		  // If the line chart should draw an area
		  showArea: false,
		  // The base for the area chart that will be used to close the area shape (is normally 0)
		  areaBase: 0,
		  // Specify if the lines should be smoothed. This value can be true or false where true will result in smoothing using the default smoothing interpolation function Chartist.Interpolation.cardinal and false results in Chartist.Interpolation.none. You can also choose other smoothing / interpolation functions available in the Chartist.Interpolation module, or write your own interpolation function. Check the examples for a brief description.
		  lineSmooth: true,
		  // If the line chart should add a background fill to the .ct-grids group.
		  showGridBackground: false,
		  // Overriding the natural low of the chart allows you to zoom in or limit the charts lowest displayed value
		  low: 0,
		  // Overriding the natural high of the chart allows you to zoom in or limit the charts highest displayed value
		  high: undefined,
		  // Padding of the chart drawing area to the container element and labels as a number or padding object {top: 5, right: 5, bottom: 5, left: 5}
		  chartPadding: {
		    top: 15,
		    right: 15,
		    bottom: 5,
		    left: 10
		  },
		  // When set to true, the last grid line on the x-axis is not drawn and the chart elements will expand to the full available width of the chart. For the last label to be drawn correctly you might need to add chart padding or offset the last label with a draw event handler.
		  fullWidth: false,
		  // If true the whole data is reversed including labels, the series order as well as the whole series data arrays.
		  reverseData: false,
		  // Override the class names that get used to generate the SVG structure of the chart
		  classNames: {
		    chart: 'ct-chart-line',
		    label: 'ct-label',
		    labelGroup: 'ct-labels',
		    series: 'ct-series',
		    line: 'ct-line',
		    point: 'ct-point',
		    area: 'ct-area',
		    grid: 'ct-grid',
		    gridGroup: 'ct-grids',
		    gridBackground: 'ct-grid-background',
		    vertical: 'ct-vertical',
		    horizontal: 'ct-horizontal',
		    start: 'ct-start',
		    end: 'ct-end'
		  }
		};
		
		<?php
			$chartData = 'var data = {
				labels: [';
			foreach($matches as $m) {$chartData .= ($m->matchNumber).',';}
			$chartData .= '],
				series: [';
			foreach($yArray as $y) {
				$chartData .= '[';
				foreach($matches as $m) {$chartData .= ($m->{$y}).',';}
				$chartData .= '],
						';
			}
			$chartData .= ']
			};
			';
			
			$chartData = preg_replace('/,([]}])/','\1',$chartData);
			print $chartData;
		?>
		new Chartist.Line('.ct-chart',data,options);
	</script>
</body>
</html>