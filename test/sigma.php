<?php
include '../includes.php';

?>



<!DOCTYPE html>
<html>
<head>
  <title>Basic sigma.js example</title>
  <style type="text/css">
    body {
      margin: 0;
    }
    #container {
      position: absolute;
      width: 100%;
      height: 100%;
    }
  </style>
</head>
<body>
  <div id="container"></div>
  <!--script>
    // Let's first initialize sigma:
    var s = new sigma('container');

    // Then, let's add some data to display:
    s.graph.addNode({
      // Main attributes:
      id: 'n0',
      label: 'Hello',
      // Display attributes:
      x: 0,
      y: 0,
      size: 1,
      color: '#f00'
    }).addNode({
      // Main attributes:
      id: 'n1',
      label: 'World !',
      // Display attributes:
      x: 1,
      y: 1,
      size: 1,
      color: '#00f'
    }).addEdge({
      id: 'e0',
      // Reference extremities:
      source: 'n0',
      target: 'n1'
    });

    // Finally, let's ask our sigma instance to refresh:
    s.refresh();
  </script-->
  <?php
if(!isset($SIGMA)){
	js('sigma/sigma.min');
	$graph = '';
	$lastNode = '';
	$lastX = 0;
	function InitGraph($graphName,$container){global $graph;
		$graph = $graphName;
		return 'var '.$graph.' = new sigma("'.$container.'");
		';}
	function LonelyNode($id,$label,$x,$y,$size,$color){global $graph;
		return $graph.'.graph.addNode({id: "'.$id.'", label: "'.$label.'", x: '.$x.', y: '.$y.', size: '.$size.', color: "'.$color.'"});
		';
	}
	function InitNode($id,$label,$x,$y,$size,$color){global $graph,$lastNode;
		$lastNode = $id;
		return LonelyNode($id,$label,$x,$y,$size,$color);
	}
	function Edge($from,$to){global $graph;
		return $graph.'.graph.addEdge({id: "'.$from.'_'.$to.'", source: "'.$from.'", target: "'.$to.'"});
		';
	}
	function TraceNode($id,$label,$x,$y,$size,$color,$lastNode){global $graph;
		$newNode = LonelyNode($id,$label,$x,$y,$size,$color);
		$newEdge = ($lastNode=='')?'':Edge($lastNode,$id);
		return $newNode.$newEdge;
	}
	function AppendNode($id,$label,$x,$y,$size,$color){global $graph,$lastNode;
		$newNode = LonelyNode($id,$label,$x,$y,$size,$color);
		$newEdge = ($lastNode=='')?'':Edge($lastNode,$id);
		$lastNode = $id;
		return $newNode.$newEdge;
	}
	function NextDatum($label,$y){global $graph,$lastX;
		$size = 1; $color = '#000';
		return ($lastX==0)?InitNode('x'.$lastX,$label,$lastX++,$y,$size,$color):AppendNode('x'.$lastX,$label,$lastX++,$y,$size,$color);
	}
	function Refresh(){global $graph;
		return $graph.'.refresh();
		';
	}
	$SIGMA = true;
}
  ?>
  <script>
	<?=InitGraph('s','container')?>
	<?=/*/NextDatum('Hello',0)/*/InitNode('n0','Hello',0,0,1,'#f00')/**/?>
	<?=/*/NextDatum('World !',1)/*/AppendNode('n1','World !',1,1,1,'#00f')/**/?>
	<?=/*/NextDatum('Foo',1.5)/*/AppendNode('n2','Foo',2,1.5,1,'#0f0')/**/?>
	<?=Refresh()?>
	</script>
</body>
</html>