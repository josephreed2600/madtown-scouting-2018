<?php

include '../includes.php';


//$EventKey = '2016hop';

if(isset($_GET['eventKey'])){$EventKey = $_GET['eventKey'];}

header("X-TBA-App-Id:team1323:php-curl:0.0;");
// create a new cURL resource
$ch = curl_init();
// set URL and other appropriate options
curl_setopt($ch, CURLOPT_URL, 'http://www.thebluealliance.com/api/v2/event/'.$EventKey.'/matches?X-TBA-App-Id=team1323:php-curl:0.1');
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// grab URL and pass it to the browser
$json = curl_exec($ch);
// close cURL resource, and free up system resources
curl_close($ch);
//print $json;
$d = json_decode($json);
//$d = $d[0];
/**
 * @param i index of match in data
 * @param c color of team /(red|blue)/
 * @param t index of team o
*/

$m = 0;


function writeMatchEntry($match,$matchNumber,$compLevel,$setNumber,$teamIndex,$teamColor,$MatchScheduleTable) {
$teamNumber = ($teamColor==1)?preg_replace('/[^0-9]/','',$match->alliances->blue->teams[$teamIndex]):preg_replace('/[^0-9]/','',$match->alliances->red->teams[$teamIndex]);
$I = 'INSERT INTO '.$MatchScheduleTable.' (matchNumber, compLevel, setNumber, teamNumber, teamColor) VALUES ('.$matchNumber.', "'.$compLevel.'", '.$setNumber.', '.$teamNumber.', '.$teamColor.')';
if(isset($_GET['printQueries'])){print $I.'<br/>';}
return q($I);
}

function deleteMatch($matchNumber,$compLevel,$setNumber,$MatchScheduleTable) {return q('DELETE FROM '.$MatchScheduleTable.' WHERE matchNumber='.$matchNumber.' AND compLevel="'.$compLevel.'" AND setNumber='.$setNumber);}

if(isset($_GET['pretty'])){print '<pre>';}

for($i=0;$i<count($d);$i++){
	$match = $d[$i];
	$matchNumber = $match->match_number;
	$compLevel = $match->comp_level;
	$setNumber = $match->set_number;
	deleteMatch($matchNumber,$compLevel,$setNumber,$MatchScheduleTable);
	writeMatchEntry($match,$matchNumber,$compLevel,$setNumber,0,0,$MatchScheduleTable);
	writeMatchEntry($match,$matchNumber,$compLevel,$setNumber,1,0,$MatchScheduleTable);
	writeMatchEntry($match,$matchNumber,$compLevel,$setNumber,2,0,$MatchScheduleTable);
	writeMatchEntry($match,$matchNumber,$compLevel,$setNumber,0,1,$MatchScheduleTable);
	writeMatchEntry($match,$matchNumber,$compLevel,$setNumber,1,1,$MatchScheduleTable);
	writeMatchEntry($match,$matchNumber,$compLevel,$setNumber,2,1,$MatchScheduleTable);
}

if(isset($_GET['show'])){include '../api/matchSchedule.php';}
if(isset($_GET['pretty'])){print '</pre>';}
?>
