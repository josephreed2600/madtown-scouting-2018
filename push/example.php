<!--http://www.developper-jeux-video.com/php-push-code/-->
<?php
include '../util.php';
?>
<html>
<head>
<?=jquery()?>
<script type="text/javascript">
  function callComplete(response) {
    $("#textDiv").empty();
    $("#textDiv").append(response);
    connect();
  };
 
  function connect() {
    $.post('longPolling.php', {}, callComplete, 'json');
  };
 
  $(document).ready( function() {
    $("#textDiv").empty();
    $("#textDiv").append("HEURE");
    connect();
  });
</script>
</head>
<body>
LONG POLLING TEST
<div id="textDiv" style="width:300; height:180; overflow:auto; border:solid 1px black;">
</div>
</body>
</html>