<!--http://www.developper-jeux-video.com/php-push-code/-->
<?php
include '../includes.php';
print html_top();
print html_usual();
print css('common');
?>

<script type="text/javascript">

var latency = 0.5;

function updateColors(c) {
	for(var i = 0; i < 6; i++) {
		$('[data-index='+i+']').attr('data-color',c[i]);
	}
}

function updateNumbers(n) {
	for(var i = 0; i < 6; i++) {
		$('[data-index='+i+']').attr('data-value',n[i]);
		$('[data-index='+i+']').text(n[i]);
	}
}

function rotate() {
	$('[data-index]').each(function(){var i = $(this).attr('data-index'); $(this).attr('data-index',5-i);})
}

  function callComplete(response) {
    setTimeout(connect,1000*latency); // delay between requests
    response = JSON.parse(response);
    var colors = [];
    switch(response.c) {
	case 0: colors = ['red','red','red','blue','blue','blue']; break;
	case 1: colors = ['red','blue','red','blue','red','blue']; break;
	case 2: colors = ['blue','red','blue','red','blue','red']; break;
	case 3: colors = ['blue','blue','blue','red','red','red']; break;
	default: break;
    }
    updateColors(colors);
    updateNumbers(response.n);
  };
 
  function connect() {
    $.post('longPolling.php', {}, callComplete, 'json');
  };
 
  $(document).ready( function() {
    $('body').on('click',rotate);
    connect();
  });
</script>

<?=html_mid()?>

<table id="input">
<tr>
	<td id="box3" class="box switch left top" data-index="3" data-value="0" data-color="blue"></td>
	<td id="box0" class="box switch right top" data-index="0" data-value="0" data-color="red"></td>
</tr>
<tr>
	<td id="box4" class="box scale left" data-index="4" data-value="0" data-color="blue"></td>
	<td id="box1" class="box scale right" data-index="1" data-value="0" data-color="red"></td>
</tr>
<tr>
	<td id="box5" class="box switch left bottom" data-index="5" data-value="0" data-color="blue"></td>
	<td id="box2" class="box switch right bottom" data-index="2" data-value="0" data-color="red"></td>

</tr>
</table>

<?php
print html_bottom();
?>