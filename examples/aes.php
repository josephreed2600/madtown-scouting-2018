<?PHP
define('AES_V1_KEY','D0QgiY8JYvx8qzKx0iaN8kwEJgwpEqAJ');
function encryptString($RAWDATA,$VERSION = 1)
{
    $key = null;
    switch($VERSION)
    {
    case 1:
    $key = AES_V1_KEY;
    break;
    }
    // encrypt string
    $td = mcrypt_module_open('rijndael-128','','ecb','');
    $iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
    mcrypt_generic_init($td,$key,$iv);
    $encrypted_string = mcrypt_generic($td, strlen($RAWDATA) . '|' .
$RAWDATA);
    mcrypt_generic_deinit($td);
    mcrypt_module_close($td);
        // base-64 encode
    return base64_encode($encrypted_string);
}

function decryptString($ENCRYPTEDDATA,$VERSION = 1,$md5Key)
{
    $key = null;
    switch($VERSION)
    {
    case 1:
    $key = md5($md5Key);
    break;
    }
    // base-64 decode
    $encrypted_string = base64_decode($ENCRYPTEDDATA);
    // decrypt string
    $td = mcrypt_module_open('rijndael-128','','ecb','');
    $iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
    mcrypt_generic_init($td,$key,$iv);
    $returned_string = mdecrypt_generic($td,$encrypted_string);
    unset($encrypted_string);
    list($length,$original_string) = explode('|',$returned_string,2);
    unset($returned_string);
    $original_string = substr($original_string,0,$length);
    mcrypt_generic_deinit($td);
    mcrypt_module_close($td);
    return $original_string; 
}

?>