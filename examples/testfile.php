<?php
include "connect.php";
include "phpmailer/class.phpmailer.php";
include "security.php";
include "aes.php";

if(isset($_GET['action'])){
	$action = remove_HTML($_GET['action']);
}
elseif(isset($_POST['action'])){
	$action = remove_HTML($_POST['action']);
}else{
	$action = "none";
}
if(isset($_GET['token']) && isset($_GET['userid'])){
		$token = remove_HTML($_GET['token']);
		$userid    = remove_HTML($_GET['userid']);
	}
elseif(isset($_POST['token']) && isset($_POST['userid'])){
	$token = remove_HTML($_POST['token']);
	$userid    = remove_HTML($_POST['userid']);
}
else{
	$token=0;
	$userid=0;
}
	switch($action){
		
		case "registerUser":
			$email = '';
			$password = '';
			if(isset($_POST['email']) && isset($_POST['password']) && isset($_POST['gamertag'])){
				$email = strtolower(remove_HTML($_POST['email']));
				$gamertag = str_replace(array("\r", "\r\n", "\n","\""),"",remove_HTML($_POST['gamertag']));
				$password = remove_HTML($_POST['password']);
				if((strlen($email)<50) && (strlen($email)>5)){
					if((strlen($gamertag)<20) && (strlen($gamertag)>5)){
						echo registerUser($email, $password,$gamertag);
					}else{
						$json = '{"userdata":[{"response": "9"}]}';
						echo $json;
					}
				}else{
					$json = '{"userdata":[{"response": "12","id": "000000","token": "000000"}]}';
					echo $json;
				}
			}else{
				$json = '{"userdata":[{"response": "11","'.$email.'","'.$gamertag.'","'.$password.'"}]}';
				echo $json;
			}
		break;
		case "checkemail":
			if(isset($_POST['email']) && isset($_POST['gamertag'])){
				$email = strtolower(remove_HTML($_POST['email']));
				$gamertag = remove_HTML($_POST['gamertag']);
				$results = mysql_fetch_array(mysql_query('select id from tduUser where email="'.$email.'" or gamertag="'.$gamertag.'"'));
				if($results[0]==''){
					$json = '{"email":[{"response": "available"}]}';
				}
				else{
					$json = '{"email":[{"response": "taken"}]}';
				}
			}
			else{
				$json = '{"email":[{"response": "invalid"}]}';
			}
			echo $json;
		break;
		
		case "login":
			$email = '';
			$password = '';
			if(isset($_POST['username']) && isset($_POST['password'])){
				$email = strtolower(remove_HTML($_POST['username']));
				$password = remove_HTML($_POST['password']);
				$json = userLogin($email, $password);
			}
			else{
				$json = '{"userdata":[{"response": "5"}]}';
			}
				
			echo $json;
			break;
		case "updateTDUStatus":
			$json = "";
			if(isValidUser($token,$userid)){
				if(isset($_POST['status'])){
					$status = remove_HTML($_POST['status']);
					mysql_query('update tduUser set tduStatus="'.$status.'" where id="'.$userid.'"') or die(mysql_error());
					$json = '{"userdata":[{"response": "1"}]}';
					if($status == 1){
						sendNoticiations($userid);
					}
				}else{
					$json = '{"userdata":[{"response": "2"}]}';
				}
			}else{
				$json = '{"userdata":[{"response": "3"}]}';
			}
			echo $json;
			break;
		case "updateProfileInfo":
			$json = "";
			if(isValidUser($token,$userid)){
				if(isset($_POST['console'])){
					$console = remove_HTML($_POST['console']);
					if(($console < 4) && ($console > 0)){
						mysql_query('update tduUser set platform="'.$console.'" where id="'.$userid.'"');
						$json = '{"userdata":[{"response": "1"}]}';
					}else{
						$json = '{"userdata":[{"response": "2"}]}';
					}
				}else{
					$json = '{"userdata":[{"response": "3"}]}';
				}
			}else{
				$json = '{"userdata":[{"response": "4"}]}';
			}
			echo $json;
			break;
		case "followUser":
			$json = "";
			if(isValidUser($token,$userid)){
				if(isset($_POST['gamertag'])){
					$gamertag = remove_HTML($_POST['gamertag']);
					$gamerId = mysql_fetch_array(mysql_query('select id from tduUser where gamertag="'.$gamertag.'"'));
					if($gamerId['id'] != ''){
						$isFollowing = mysql_fetch_array(mysql_query('select id from tduFollowers where userid="'.$userid.'" and followid="'.$gamerId['id'].'"'));
						if($isFollowing['id'] != ''){
							mysql_query('delete from tduFollowers where id="'.$isFollowing['id'].'"');
							$json = '{"userdata":[{"response": "1"}]}';	
						}else{
							mysql_query('insert into tduFollowers (userid,followid) values("'.$userid.'","'.$gamerId['id'].'")');
							$json = '{"userdata":[{"response": "2"}]}';
						}						
					}else{
						$json = '{"userdata":[{"response": "3"}]}';
					}					
				}else{
					$json = '{"userdata":[{"response": "4"}]}';
				}
			}else{
				$json = '{"userdata":[{"response": "5"}]}';
			}
			echo $json;
			break;
		case "sendChallenge":
			$json = "";
			if(isValidUser($token,$userid)){
				if(isset($_POST['gamertag']) && isset($_POST['challengetype'])){
					$gamertag = remove_HTML($_POST['gamertag']);
					$challengetype = remove_HTML($_POST['challengetype']);
					$toid = mysql_fetch_array(mysql_query('select id,notKey from tduUser where gamertag="'.$gamertag.'"'));
					$mygamertag = mysql_fetch_array(mysql_query('select gamertag from tduUser where id="'.$userid.'"')) or die(mysql_error());
					if($toid['id'] != ''){
						mysql_query('insert into tduChallenges (toid,fromid,timestamp,challengetype) values("'.$toid['id'].'","'.$userid.'","'.time().'","'.$challengetype.'")');
						if($toid['notKey']!=''){
							sendNotification("Challenge Sent", $mygamertag['gamertag'].' has sent you a request',$toid['notKey']);							
						}
						$json = '{"userdata":[{"response": "1"}]}';	
					}else{
						$json = '{"userdata":[{"response": "2"}]}';	
					}
				}else{
					$json = '{"userdata":[{"response": "3"}]}';	
				}
			}else{
				$json = '{"userdata":[{"response": "4"}]}';	
			}
			echo $json;
				break;
		case "acceptChallenges":
			$json = "";
			if(isValidUser($token,$userid)){
				if(isset($_POST['gamertag']) && isset($_POST['challengetype'])){
					$gamertag = remove_HTML($_POST['gamertag']);
					$challengetype = remove_HTML($_POST['challengetype']);
					$fromid = mysql_fetch_array(mysql_query('select id,notKey from tduUser where gamertag="'.$gamertag.'"')) or die(mysql_error());
					$mygamertag = mysql_fetch_array(mysql_query('select gamertag from tduUser where id="'.$userid.'"')) or die(mysql_error());
					if($fromid['id'] != ''){
						mysql_query('update tduChallenges set accepted=1 where toid="'.$userid.'" and fromid="'.$fromid['id'].'" and challengetype="'.$challengetype.'"') or die(mysql_error());
						if($fromid['notKey']!=''){
							sendNotification("Challenge Accepted", $mygamertag['gamertag'].' has accepted your request',$fromid['notKey']);							
						}
						$json = '{"userdata":[{"response": "1"}]}';	
					}else{
						$json = '{"userdata":[{"response": "2"}]}';	
					}
				}else{
					$json = '{"userdata":[{"response": "3"}]}';	
				}
			}else{
				$json = '{"userdata":[{"response": "4"}]}';	
			}
			echo $json;
				break;
		case "denyChallenges":
			$json = "";
			if(isValidUser($token,$userid)){
				if(isset($_POST['gamertag']) && isset($_POST['challengetype'])){
					$gamertag = remove_HTML($_POST['gamertag']);
					$challengetype = remove_HTML($_POST['challengetype']);
					$fromid = mysql_fetch_array(mysql_query('select id,notKey from tduUser where gamertag="'.$gamertag.'"')) or die(mysql_error());
					$mygamertag = mysql_fetch_array(mysql_query('select gamertag from tduUser where id="'.$userid.'"')) or die(mysql_error());
					if($fromid['id'] != ''){
						mysql_query('delete from tduChallenges where toid="'.$userid.'" and fromid="'.$fromid['id'].'" and challengetype="'.$challengetype.'"') or die(mysql_error());
						if($fromid['notKey']!=''){
							sendNotification("Challenge Denied", $mygamertag['gamertag'].' has denied your request',$fromid['notKey']);							
						}
						$json = '{"userdata":[{"response": "1"}]}';	
					}else{
						$json = '{"userdata":[{"response": "2"}]}';	
					}
				}else{
					$json = '{"userdata":[{"response": "3"}]}';	
				}
			}else{
				$json = '{"userdata":[{"response": "4"}]}';	
			}
			echo $json;
				break;
		case "ignoreChallenges":
			$json = "";
			if(isValidUser($token,$userid)){
				if(isset($_POST['gamertag']) && isset($_POST['challengetype'])){
					$gamertag = remove_HTML($_POST['gamertag']);
					$challengetype = remove_HTML($_POST['challengetype']);
					$fromid = mysql_fetch_array(mysql_query('select id from tduUser where gamertag="'.$gamertag.'"')) or die(mysql_error());
					$mygamertag = mysql_fetch_array(mysql_query('select gamertag from tduUser where id="'.$userid.'"')) or die(mysql_error());
					if($fromid['id'] != ''){
						mysql_query('delete from tduChallenges where toid="'.$userid.'" and fromid="'.$fromid['id'].'" and challengetype="'.$challengetype.'"') or die(mysql_error());
						$json = '{"userdata":[{"response": "1"}]}';	
					}else{
						$json = '{"userdata":[{"response": "2"}]}';	
					}
				}else{
					$json = '{"userdata":[{"response": "3"}]}';	
				}
			}else{
				$json = '{"userdata":[{"response": "4"}]}';	
			}
			echo $json;
				break;
		case "getChallenges":
			$json = "";
			if(isValidUser($token,$userid)){
				
				$challenges = mysql_query('select * from tduChallenges where toid="'.$userid.'" and accepted=0');
				$num = mysql_num_rows($challenges);
				$json = '{"results":[{"response": "'.mysql_num_rows($challenges).'", "users": {"userData": [';
				while($challenge = mysql_fetch_array($challenges)){
					$gamertag = mysql_fetch_array(mysql_query('select gamertag from tduUser where id="'.$challenge['fromid'].'"'));
					$json .= '{"gamertag": "'.$gamertag['gamertag'].'",'.
									'"timestamp": "'.$challenge['timestamp'].'",'.
									'"challengetype": "'.$challenge['challengetype'].'"},';
				}
				$json .= ']}}]}';
				$json = str_replace(',]', ']',$json);
				if($num == 0){
					$json = '{"results":[{"response": "0"}]}';
				}
			}else{
				$json = '{"results":[{"response": "0"}]}';
			}
			echo $json;
				break;
		case "getMessages":
			$json = "";
			if(isValidUser($token,$userid)){
				
				$messages = mysql_query('select * from tduMessaging where toid="'.$userid.'" order by timestamp ASC');
				$num = mysql_num_rows($messages);
				if($num == 0){
					$json = '{"results":[{"response": "0"}]}';
				}else{
					$json = '{"results":[{"response": "'.$num.'", "users": {"userData": [';
				while($message = mysql_fetch_array($messages)){
					$gamertag = mysql_fetch_array(mysql_query('select gamertag from tduUser where id="'.$message['fromid'].'"'));
					$json .= '{"gamertag": "'.str_replace(array("\r", "\r\n", "\n","\""),"",$gamertag['gamertag']).'",'.
									'"timestamp": "'.$message['timestamp'].'",'.
									'"message": "'.str_replace(array("\r", "\r\n", "\n","\""),"",$message['message']).'",'.
									'"msgid": "'.$message['id'].'",'.
									'"isread": "'.$message['isread'].'",'.
									'"subject": "'.str_replace(array("\r", "\r\n", "\n","\""),"",$message['subject']).'"},';
				}
				$json .= ']}}]}';
				$json = str_replace(',]', ']',$json);
				}
			}else{
				$json = '{"results":[{"response": "0"}]}';
			}
			echo $json;
				break;
		case "sendMessage":
			$json = "";
			if(isValidUser($token,$userid)){
				if(isset($_POST['gamertag']) && isset($_POST['subject']) && isset($_POST['message'])){
					$gamertag = remove_HTML($_POST['gamertag']);
					$subject  = remove_HTML($_POST['subject']);
					$message  = remove_HTML($_POST['message']);
					$toid = mysql_fetch_array(mysql_query('select id,notKey from tduUser where gamertag="'.$gamertag.'"'));
					$fromid = mysql_fetch_array(mysql_query('select gamertag from tduUser where id="'.$userid.'"'));
					if($toid['id'] != ''){
						mysql_query('insert into tduMessaging (subject,toid,fromid,message,timestamp) values("'.$subject.'","'.$toid['id'].'","'.$userid.'", "'.$message.'","'.time().'")');
						$json = '{"results":[{"response": "1"}]}';
						if($toid['notKey'] != ''){
							sendNotification("New message from ".$fromid['gamertag'], $message,$toid['notKey']);
						}
					}else{
						$json = '{"results":[{"response": "2"}]}';
					}
				}else{
					$json = '{"results":[{"response": "0"}]}';
				}
			}else{
				$json = '{"results":[{"response": "0"}]}';
			}
			echo $json;
			break;
		case "messageRead":
			if(isValidUser($token,$userid)){
				if(isset($_POST['msgid'])){
					$msgid = remove_HTML($_POST['msgid']);
					mysql_query('update tduMessaging set isread=1 where id="'.$msgid.'"');
				}
			}
			break;
		case "deleteMessage":
			if(isValidUser($token,$userid)){
				if(isset($_POST['msgid'])){
					$msgid = remove_HTML($_POST['msgid']);
					mysql_query('delete from tduMessaging where id="'.$msgid.'" and toid="'.$userid.'"');
				}
			}
			break;
		case "getCruisers":
			$json = "";
			if(isValidUser($token,$userid)){
				$follow = mysql_query('select * from tduCruises where userid IN (select followid from tduFollowers where userid="'.$userid.'") order by timestamp desc limit 0,100');
				$notfollow = mysql_query('select * from tduCruises where userid NOT IN (select followid from tduFollowers where userid="'.$userid.'") order by timestamp desc limit 0,100');
				$json = '{"results":[{"response": "'.mysql_num_rows($notfollow).'", "users": {"userData": [';
				while($info = mysql_fetch_array($notfollow)){
					$userinfo = mysql_fetch_array(mysql_query('select tduStatus,gamertag,platform from tduUser where id="'.$info['userid'].'"'));
					$json .= '{"gamertag": "'.str_replace(array("\r", "\r\n", "\n","\""),"",$userinfo['gamertag']).'",'.
									'"timestamp": "'.$info['timestamp'].'",'.
									'"message": "'.str_replace(array("\r", "\r\n", "\n","\""),"",$info['message']).'",'.
									'"following": "0",'.
									'"platform": "'.$userinfo['platform'].'",'.
									'"status": "'.$userinfo['tduStatus'].'"},';
				}
				while($info = mysql_fetch_array($follow)){
					$userinfo = mysql_fetch_array(mysql_query('select tduStatus,gamertag,platform from tduUser where id="'.$info['userid'].'"'));
					$json .= '{"gamertag": "'.$userinfo['gamertag'].'",'.
									'"timestamp": "'.$info['timestamp'].'",'.
									'"message": "'.$info['message'].'",'.
									'"following": "1",'.
									'"platform": "'.$userinfo['platform'].'",'.
									'"status": "'.$userinfo['tduStatus'].'"},';
				}
				$json .= ']}}]}';
				$json = str_replace(',]', ']',$json);
			}else{
				$json = '{"results":[{"response": "0"}]}';
			}
			echo $json;
			break;
			
		case "addCruise":
			$json = "";
			if(isValidUser($token,$userid)){
				if(isset($_POST['message'])){
					$message = remove_HTML($_POST['message']);
					mysql_query('delete from tduCruises where userid="'.$userid.'"');
					mysql_query('update tduUser set tduStatus="1" where id="'.$userid.'"') or die(mysql_error());
					mysql_query('insert into tduCruises (timestamp,message,userid) values("'.time().'","'.$message.'","'.$userid.'")');
					$json = '{"results":[{"response": "1"}]}';
				}else{
					$json = '{"results":[{"response": "2"}]}';
				}				
			}else{
				$json = '{"results":[{"response": "0"}]}';
			}
			echo $json;
		break;
		
		case "resetPassword":
			if(isset($_POST['email'])){
					$email = remove_HTML($_POST['email']);
					$isUser = mysql_fetch_array(mysql_query('select id from tduUser where email="'.$email.'"'));
					if($isUser[0]!=''){
						$tempPassword = generateInfo(9);
						$resetLink    = generateInfo(19);
						mysql_query('update tduUser set tempPassword="'.$tempPassword.'",resetLink="'.$resetLink.'" where id="'.$isUser['id'].'"');
						$mail = new PHPMailer();
						$mail->IsSMTP(); // set mailer to use SMTP
						$mail->Host = "ssl://smtp.gmail.com"; // specify main and backup server
						$mail->Port = 465; // set the port to use
						$mail->SMTPAuth = true; // turn on SMTP authentication
						$mail->Username = "tdustats@gorohi.com"; // your SMTP username or your gmail username
						$mail->Password = "Z5A6K8S9"; // your SMTP password or your gmail password
						$from = "tdustats@gorohi.com"; // Reply to this email
						$to=$email; // Recipients email ID
						$name="Test Drive Unlimited 2 Stats"; // Recipient's name
						$mail->From = $from;
						$mail->FromName = "Test Drive Unlimited 2 Stats"; // Name to indicate where the email came from when the recepient received
						$mail->AddAddress($to,$name);
						$mail->AddReplyTo($from,"Test Drive Unlimited 2 Stats");
						$mail->WordWrap = 50; // set word wrap
						$mail->IsHTML(true); // send as HTML
						$mail->Subject = "Test Drive Unlimited 2 Stats Password Reset";
						$mail->Body = 'A request to reset your Test Drive Unlimited 2 Stats password has been made. To reset your password click the link below.
						<a href="http://tdustats.gorohi.com/reset.php?link='.$resetLink.'">Click to Reset</a> <br>Your temperary password is: '.$tempPassword.'. If you believe you received this as an error please ignore the message'; //HTML Body
						$mail->AltBody = "This is the body when user views in plain text format"; //Text Body
						if(!$mail->Send())
						{
						echo $json = '{"userdata":[{"response": "Error"}]}';
						}
						else
						{
						echo $json = '{"userdata":[{"response": "Success"}]}';
						}
					}
					else{
						echo $json = '{"userdata":[{"response": "Error"}]}';
					}
			}
		break;
		case "testing":
			sendNoticiations(2);
			break;
				
	}
	
	
	
function registerUser($email, $password,$gamertag){
	
		if(check_email_address($email)){
			$results = mysql_fetch_array(mysql_query('select id from tduUser where email="'.$email.'" or gamertag="'.$gamertag.'"'));
			if($results[0]==''){
				$salted = md5($email).':'.md5($password);
				mysql_query('insert into tduUser (email,password,gamertag) values("'.$email.'","'.$salted.'","'.$gamertag.'")') or die(mysql_error());
				$users = mysql_fetch_array(mysql_query('select id from tduUser where email="'.$email.'"'));			
				$json = userLogin($email, $password);
				return $json;
			}
			else{
				$json = '{"userdata":[{"response": "0"}]}';
				return $json;
			}
		}
		else{
		$json = '{"userdata":[{"response": "0"}]}';
		return $json;
		}
}

function userLogin($email,$password){
	$salted = md5($email).':'.md5($password);
	$ip=$_SERVER['REMOTE_ADDR'];
	$token = md5($email.':'.time());
	$userDetails = mysql_fetch_array(mysql_query('select id,password,loginAttempts,email,tduStatus,platform,gamertag from tduUser where email="'.$email.'"'));
	
		if($userDetails['password']!=''){
			if($userDetails['loginAttempts']<20){
				if($salted==$userDetails['password']){
					$tSeed = rand(1,5000);
					if(isset($_POST['sessKey'])){
						$sessionKey = remove_HTML($_POST['sessKey']);
					}else{
						$sessionKey = 0;
					}
					mysql_query('update tduUser set lastip="'.$ip.'",loginAttempts=0,lastTimeStamp="'.time().'",tSeed="'.$tSeed.'",notKey="'.$sessionKey.'" where email="'.$email.'"');
					
					return '{"userdata":[{"response": "1","id": "'.$userDetails['id'].'","tSeed": "'.$tSeed.'","status": "'.$userDetails['tduStatus'].'","platform": "'.$userDetails['platform'].'","gamertag": "'.$userDetails['gamertag'].'"}]}';
				}
				else{
					if($userDetails['email']==$email){
						$attempts = $userDetails['loginAttempts']+1;
						mysql_query('update tduUser set loginAttempts="'.$attempts.'" where email="'.$email.'"') or die(mysql_error());
					}
					return '{"userdata":[{"response": "3"}]}';
				}
			}
			return '{"userdata":[{"response": "4"}]}';
		}
	return '{"userdata":[{"response": "10"}]}';
}

function sendNoticiations($userid){
$gamertag = mysql_fetch_array(mysql_query('select gamertag from tduUser where id="'.$userid.'"'));
$users = mysql_query('select userid from tduFollowers where followid="'.$userid.'"');
$count = 0;
$tokens = '';
while($user = mysql_fetch_array($users)){
	
	$userinfo = mysql_fetch_array(mysql_query('select notKey from tduUser where id="'.$user['userid'].'" and notKey<>"0"'));
	if($userinfo['notKey'] != ''){
		$tokens .= '<token>'.$userinfo['notKey'].'</token>';
		$count = $count + 1;
	}
}	
if($count > 0){
	$ch = curl_init();
// URL for curl
$url = "http://notify.xtify.com/api/1.2/pn/push";
$string = '<?xml version="1.0" encoding="UTF-8"?>
                <push-notification> 
                <android>
                <appKey>ab9ddb86-d730-42ba-ad22-55ba1422a315</appKey>
                <secret>46b9d305-559e-4108-8ca6-3aead7d1cf2c</secret>
                <deviceTokens>'.$tokens.'</deviceTokens>
                <messageTitle>Player Online</messageTitle>
                <message>'.$gamertag['gamertag'].' has logged in to TDU 2</message>
                <actionType>LAUNCH_APP</actionType>
                </android>
                </push-notification>';
// Clean up string
$putString = stripslashes($string);
// Put string into a temporary file
$putData = tmpfile();
// Write the string to the temporary file
fwrite($putData, $putString);
// Move back to the beginning of the file
fseek($putData, 0);

// Headers
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/xml"));
// Binary transfer i.e. --data-BINARY
curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, $url);
// Using a PUT method i.e. -XPUT
curl_setopt($ch, CURLOPT_PUT, true);
// Instead of POST fields use these settings
curl_setopt($ch, CURLOPT_INFILE, $putData);
curl_setopt($ch, CURLOPT_INFILESIZE, strlen($putString));

$output = curl_exec($ch);
//echo $output;
// Close the file
fclose($putData);
// Stop curl
curl_close($ch);

}

}

function check_email_address($email) {
	
  // First, we check that there's one @ symbol, 
  // and that the lengths are right.
  if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
    // Email invalid because wrong number of characters 
    // in one section or wrong number of @ symbols.
    return false;
  }
  // Split it into sections to make life easier
  $email_array = explode("@", $email);
  $local_array = explode(".", $email_array[0]);
  for ($i = 0; $i < sizeof($local_array); $i++) {
    if
(!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&
↪'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$",
$local_array[$i])) {
      return false;
    }
  }
  // Check if domain is IP. If not, 
  // it should be valid domain name
  if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
    $domain_array = explode(".", $email_array[1]);
    if (sizeof($domain_array) < 2) {
        return false; // Not enough parts to domain
    }
    for ($i = 0; $i < sizeof($domain_array); $i++) {
      if
(!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|
↪([A-Za-z0-9]+))$",
$domain_array[$i])) {
        return false;
      }
    }
  }
  return true;
}

function sendNotification($title,$message,$notKey){
		$tokens = '<token>'.$notKey.'</token>';
		$ch = curl_init();
		// URL for curl
		$url = "http://notify.xtify.com/api/1.2/pn/push";
		$string = '<?xml version="1.0" encoding="UTF-8"?>
		                <push-notification> 
		                <android>
		                <appKey>ab9ddb86-d730-42ba-ad22-55ba1422a315</appKey>
		                <secret>46b9d305-559e-4108-8ca6-3aead7d1cf2c</secret>
		                <deviceTokens>'.$tokens.'</deviceTokens>
		                <messageTitle>'.$title.'</messageTitle>
		                <message>'.$message.'</message>
		                <actionType>LAUNCH_APP</actionType>
		                </android>
		                </push-notification>';
		// Clean up string
		$putString = stripslashes($string);
		// Put string into a temporary file
		$putData = tmpfile();
		// Write the string to the temporary file
		fwrite($putData, $putString);
		// Move back to the beginning of the file
		fseek($putData, 0);
		
		// Headers
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/xml"));
		// Binary transfer i.e. --data-BINARY
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		// Using a PUT method i.e. -XPUT
		curl_setopt($ch, CURLOPT_PUT, true);
		// Instead of POST fields use these settings
		curl_setopt($ch, CURLOPT_INFILE, $putData);
		curl_setopt($ch, CURLOPT_INFILESIZE, strlen($putString));
		
		$output = curl_exec($ch);
		//echo $output;
		// Close the file
		fclose($putData);
		// Stop curl
		curl_close($ch);
}
?>