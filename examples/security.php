<?php

function remove_HTML($s , $keep = '' , $expand = 'script|style|noframes|select|option'){
        //prep the string
        $s = ' ' . $s;
        $k = '';
		$k[0]='';
        //initialize keep tag logic
        if(strlen($keep) > 0){
            $k = explode('|',$keep);
            for($i=0;$i<count($k);$i++){
                $s = str_replace('<' . $k[$i],'[{(' . $k[$i],$s);
                $s = str_replace('</' . $k[$i],'[{(/' . $k[$i],$s);
            }
        }
       
        //begin removal
        //remove comment blocks
        while(stripos($s,'<!--') > 0){
            $pos[1] = stripos($s,'<!--');
            $pos[2] = stripos($s,'-->', $pos[1]);
            $len[1] = $pos[2] - $pos[1] + 3;
            $x = substr($s,$pos[1],$len[1]);
            $s = str_replace($x,'',$s);
        }
       
        ///remove tags with content between them
        if(strlen($expand) > 0){
            $e = explode('|',$expand);
            for($i=0;$i<count($e);$i++){
                while(stripos($s,'<' . $e[$i]) > 0){
                    $len[1] = strlen('<' . $e[$i]);
                    $pos[1] = stripos($s,'<' . $e[$i]);
                    $pos[2] = stripos($s,$e[$i] . '>', $pos[1] + $len[1]);
                    $len[2] = $pos[2] - $pos[1] + $len[1];
                    $x = substr($s,$pos[1],$len[2]);
                    $s = str_replace($x,'',$s);
                }
            }
        }
       
        //remove remaining tags
        while(stripos($s,'<') > 0){
            $pos[1] = stripos($s,'<');
            $pos[2] = stripos($s,'>', $pos[1]);
            $len[1] = $pos[2] - $pos[1] + 1;
            $x = substr($s,$pos[1],$len[1]);
            $s = str_replace($x,'',$s);
        }
       
        //finalize keep tag
        for($i=0;$i<count($k);$i++){
            $s = str_replace('[{(' . $k[$i],'<' . $k[$i],$s);
            $s = str_replace('[{(/' . $k[$i],'</' . $k[$i],$s);
        }
       
        return trim(mysql_real_escape_string($s));
    }
	
	
function generateInfo($length)
{

  // start with a blank password
  $randfilename = "";

  // define possible characters
  $possible = "0123456789bcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
    
  // set up a counter
  $i = 0; 
    
  // add random characters to $password until $length is reached
  while ($i < $length) { 

    // pick a random character from the possible ones
    $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
        
    // we don't want this character if it's already in the password
    if (!strstr($randfilename, $char)) { 
      $randfilename .= $char;
      $i++;
    }

  }

  // done!
  return $randfilename;

}

function isValidUser($token,$id){	
		

		$ip=$_SERVER['REMOTE_ADDR'];
		
		 
		$tokenCheck = mysql_fetch_array(mysql_query('select lastip,loginAttempts,tSeed from tduUser where id="'.$id.'"'));
		
		$key = decryptString($token,1,$tokenCheck['tSeed']);
		
		if(($key == $tokenCheck['tSeed']) && ($tokenCheck['lastip'] == $ip) && ($tokenCheck['loginAttempts'] < 20)){
			$tokenCheck['tSeed'] = $tokenCheck['tSeed'] + 1;
			mysql_query('update tduUser set tSeed="'.$tokenCheck['tSeed'].'" where id="'.$id.'"');
			return true;
		}else{
			return false;
		}
}

/*
function updateSecurityToken($userid){
	$ip=$_SERVER['REMOTE_ADDR'];
	$newToken = generateNewToken($userid);
	mysql_query('update domUser set lastip="'.$ip.'",loginAttempts=0,timestamp="'.time().'",token="'.$newToken.'" where id="'.$userid.'"');
}*/
?>