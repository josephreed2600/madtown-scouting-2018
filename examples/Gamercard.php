<?php


/**
 * Gamercard utility class
 *
 * A utility class for generating Gamercards and retrieving structured data
 * about the defined Gamertag.
 */
class Gamercard {
  
  private $url;
  private $gamertag;
  private $xboxinfo_url;
  
  function Gamercard($gamertag, $url = NULL, $xboxinfo_url = NULL) {
    $this->gamertag = $gamertag;
    $this->xboxinfo_url = $xboxinfo_url;
    $this->url = $url ? $url : 'http://gamercard.xbox.com/%gamertag.card';
  }
  
  /**
   * Generate a URL to the gamertag's gamercard
   */
  function url() {
    return strtr($this->url, array('%gamertag' => rawurlencode($this->gamertag)));
  }
  
  /**
   * Generate the HTML that can be used to embed a gamertag's gamercard
   */
  function render() {
    $out = "<iframe src=\"". $this->url() ."\" 
              scrolling=\"no\" frameBorder=\"0\" height=\"140\" width=\"204\">
              ". $this->gamertag ."
            </iframe>";
    return $out;
  }
  
  /**
   * Get structured data about the gamertag.
   * @see XboxInfo class
   */
  function data() {
    require_once('XboxInfo.php');
    $xbox = new XboxInfo($this->xboxinfo_url);
    return $xbox->getXboxInfo($this->gamertag);
  }
  
}


?>