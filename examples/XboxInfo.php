<?php


/**
 * XboxInfo SOAP Client
 *
 * Utilizes the SOAP web service provided by Duncan Mackenzie at
 * http://duncanmackenzie.net/services/XboxInfo.asmx
 */
class XboxInfo {
  
  private $url;
  private $client;
  
  /**
   * @param $url
   *   (Optional) The SOAP webservice URL. Defaults to the service URL provided by Duncan Mackenzie.
   */
  function XboxInfo($url = NULL) {
    $this->url = $url ? $url : 'http://duncanmackenzie.net/services/XboxInfo.asmx?WSDL';
    
    try {
      $this->client = new SoapClient($this->url);
    }
    catch (SoapFault $fault) {
      $this->_soapFault($fault);
      throw new XboxInfoConnectionException("Unable to connect to XboxInfo Service.  Verify the url property is defined and that the host is reachable.");
    }
  }
  
  /**
   * General SOAP fault handler
   *
   * @param $fault
   *   The SOAP exception.
   */
  private function _soapFault($fault) {
    throw new XboxInfoSoapException("SOAP Fault: \n  faultcode: {$fault->faultcode}, \n  faultstring: {$fault->faultstring}");
  }
  
  /**
   * Retrieve an XboxInfo object from the Xbox Info web service.
   *
   * @param $gamertag
   *   The user's gamertag
   * @return
   *   If successful, an object containing the full details of the Gamertag, including profile
   *   details, GamerScore and recent games.  Otherwise, returns NULL.
   */ 
  function getXboxInfo($gamertag) {
    $params = array('gamertag' => $gamertag);
    try {
      $resp = $this->client->GetXboxInfo($params);
      if (isset($resp->GetXboxInfoResult)) {
        return $resp->GetXboxInfoResult;
      }
      return NULL;
    }
    catch (SoapFault $fault) {
      $this->_soapFault($fault);
    }
    return NULL;
  }
  
}


/**
 * General SOAP Exception.
 */
class XboxInfoSoapException extends Exception {}


/**
 * Exception used to indicate a problem connecting to the XboxInfo server.
 */
class XboxInfoConnectionException extends Exception {}


?>