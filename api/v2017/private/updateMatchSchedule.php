<?php
include '../../includes.php';

if(get('eventKey')=='' || get('table')=='' || get('key')!='foo'){
	print 
'<pre>Call with GET and parameters eventKey and table.
*** ONLY USE THIS IF YOU KNOW WHAT YOU ARE DOING! ***
If you\'re seeing this message, then you probably don\'t.
Line 14 includes a DROP TABLE, so caution is crucial.</pre>';
echo '// I wonder if I should have written better documentation, if accuracy is so critical';
} else {
	$EventKey = get('eventKey');
	$MatchScheduleTable = get('table');
	
	$s = 'DROP TABLE IF EXISTS '.$MatchScheduleTable;
//	print $s.'<br/>';
	q($s);
	$s = 'CREATE TABLE '.$MatchScheduleTable.' (compLevel varchar(2), setNumber tinyint, matchNumber tinyint, teamNumber smallint, teamColor tinyint, notes varchar(8000))';
//	print $s.'<br/>';
	q($s);
	
	header("X-TBA-App-Id:team1323:php-curl:0.0;");
	// create a new cURL resource
	$ch = curl_init();
	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, 'http://www.thebluealliance.com/api/v2/event/'.$EventKey.'/matches?X-TBA-App-Id=team1323:api:0.2');
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// grab URL and pass it to the browser
	$json = curl_exec($ch);
	// close cURL resource, and free up system resources
	curl_close($ch);
	$d = json_decode($json);

	function writeMatchEntry($match,$compLevel,$setNumber,$matchNumber,$teamIndex,$teamColor,$MatchScheduleTable) {
	$teamNumber = ($teamColor==1)?preg_replace('/[^0-9]/','',$match->alliances->blue->teams[$teamIndex]):preg_replace('/[^0-9]/','',$match->alliances->red->teams[$teamIndex]);
	$I = 'INSERT INTO '.$MatchScheduleTable.' (compLevel, setNumber, matchNumber, teamNumber, teamColor) VALUES ("'.$compLevel.'", '.$setNumber.', '.$matchNumber.', '.$teamNumber.', '.$teamColor.')';
	if(isget('printQueries')) print $I.'<br/>';
	return q($I);
	}
		
	function deleteMatch($matchNumber,$compLevel,$setNumber,$MatchScheduleTable) {
		$D = 'DELETE FROM '.$MatchScheduleTable.' WHERE matchNumber='.$matchNumber.' AND compLevel="'.$compLevel.'" AND setNumber='.$setNumber;
		if(isget('printQueries')) print $D.'<br/>';
		return q($D);
	}

	if(isset($_GET['pretty'])){print '<pre>';}

	for($i=0;$i<count($d);$i++){
		$match = $d[$i];
		$matchNumber = $match->match_number;
		$compLevel = $match->comp_level;
		$setNumber = $match->set_number;
		deleteMatch($matchNumber,$compLevel,$setNumber,$MatchScheduleTable);
		foreach (array(0,1) as $teamColor) {
			foreach (array(0,1,2) as $teamIndex) {
				writeMatchEntry($match,$compLevel,$setNumber,$matchNumber,$teamIndex,$teamColor,$MatchScheduleTable);
			}
		}
		/*/
			writeMatchEntry($match,$matchNumber,$compLevel,$setNumber,0,0,$MatchScheduleTable);
			writeMatchEntry($match,$matchNumber,$compLevel,$setNumber,1,0,$MatchScheduleTable);
			writeMatchEntry($match,$matchNumber,$compLevel,$setNumber,2,0,$MatchScheduleTable);
			writeMatchEntry($match,$matchNumber,$compLevel,$setNumber,0,1,$MatchScheduleTable);
			writeMatchEntry($match,$matchNumber,$compLevel,$setNumber,1,1,$MatchScheduleTable);
			writeMatchEntry($match,$matchNumber,$compLevel,$setNumber,2,1,$MatchScheduleTable);
		/**/
	}

	if(isset($_GET['show']) || isset($_GET['pretty'])){include '../matchSchedule.php';}
	if(isset($_GET['pretty'])){print '</pre>';}
}
?>