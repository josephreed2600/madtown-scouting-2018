<?php
include 'includes.php';

// 2018-01-31: Instead of complaining about not receiving a FQEK,
// we'll just supply the current event's
/*/
if(!isget('FQEK')) {
	echo 'fetchData.php did not receive a FQEK';
	exit();
}
/**/
if(isget('FQEK')) $FQEK = $_GET['FQEK'];

$ek = array();

if(!preg_match('/([0-9]{4,})([a-z]{1,4})/',$FQEK,$ek)) {
	echo 'fetchAverages.php did not receive a valid FQEK: '.$FQEK;
	exit();
}

$FQEK = $ek[0];
$Year = $ek[1];
$EventKey = $ek[2];

$file = $ROOT.'/'.$Year.'/'.$EventKey.'/tabledef_data';

if(!is_readable($file)){
	echo 'fetchAverages.php could not read the requested event: '.$FQEK;
	exit();
}

$tbl = parseJSONfile($file);
$tableName = $tbl->tableName;
$cols = $tbl->columns;
$q = '';
$qEnd = ' GROUP BY ';
$ksep = '';
$gsep = '';
foreach($cols as $c) {
	if(hasClass($c,'average')) {
		$q .= $ksep."AVG(".$c->field.") AS ".$c->field;
		$ksep = ', ';
	}
	if(hasClass($c,'notaverage')) {
		$q .= $ksep.$c->field;
		$ksep = ', ';
		$qEnd .= $gsep.$c->field;
		$gsep = ', ';
	}
}
if($qEnd == ' GROUP BY ') $qEnd = '';
$q = 'SELECT '.$q.' FROM '.$tableName.$qEnd;
$query = dbq($q);
$json = '[';
$osep = '';
if($query) while($row = dbfetch($query)) {
	$json .= $osep.'{';
	$ksep = '';
	foreach($cols as $c) {
		if(isset($row[$c->field])) {
			$json .= $ksep.'"'.$c->field.'" : "'.$row[$c->field].'"';
			$ksep = ', ';
		}
	}
	$json .= '}';
	$osep = ', ';
}
$json .= ']';
print $json;