<?php
include 'includes.php';

$tableName = $MatchScheduleTable;

if(isset($_REQUEST['FQEK'])) $tableName = 'schedule'.$_REQUEST['FQEK'];

//$cols = array('Year','EventKey','FQEK','EventName','SeasonName','SeasonSegment','matchNumber','teamNumber','color');
$cols = array('matchNumber','teamNumber','color');
$query = dbq('SELECT * FROM '.$tableName);
$json = '[';
$osep = '';
if($query) while($row = dbfetch($query)) {
	$matchNumber = $row['matchNumber'];
	$teamNumber  = $row['teamNumber'];
	$color       = $row['color'];
	
	$json .= $osep.'{';
	$ksep = '';
	foreach($cols as $c) {
		$json .= $ksep.'"'.$c.'" : "'.eval('return $'.$c.';').'"'; // FIXME: This is returning each record in its own object. Maybe try a series of select calls (prepared?) with 'where matchNumber = '.i++ or such
		$ksep = ', ';
	}
	$json .= '}';
	$osep = ', ';
}
$json .= ']';
print $json;

?>