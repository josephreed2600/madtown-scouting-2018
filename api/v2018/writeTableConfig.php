<?php
include 'includes.php';

if(!isset($_POST['data'])) {
	echo __FILE__.' received no data';
	exit();
}

$json = $_POST['data'];

if(json_decode($json)===false) {
	echo __FILE__.' received invalid JSON: '.json_last_error_msg();
	exit();
}

$data = json_decode($json);

$Year = $data->Year;

$tabletype = (isset($_POST['table']))?$_POST['table']:'data';

$file = $ROOT.'/'.$Year.'/tabledef_'.$tabletype;			// was just tabledef_data

if(!is_writable($file)) {
	echo __FILE__.' could not open file '.$file.' for writing';
	exit();
}

if(file_put_contents($file,$json)===false) {
	echo __FILE__.' failed to write data to '.$file;
	exit();
} else {
	echo __FILE__.': success';
}