<?php
include 'includes.php';

if(!isget('FQEK')) {
	echo 'fetchData.php did not receive a FQEK';
	exit();
}

$FQEK = $_GET['FQEK'];
$ek = array();

if(!preg_match('/([0-9]{4,})([a-z]{1,4})/',$FQEK,$ek)) {
	echo 'fetchData.php did not receive a valid FQEK: '.$FQEK;
	exit();
}

$FQEK = $ek[0];
$Year = $ek[1];
$EventKey = $ek[2];

$file = $ROOT.'/'.$Year.'/'.$EventKey.'/tabledef_data';

if(!is_readable($file)){
	echo 'fetchData.php could not read the requested event: '.$FQEK;
	exit();
}

$tbl = parseJSONfile($file);
$json = json_encode($tbl);

print $json;