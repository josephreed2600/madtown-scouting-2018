<?php

include 'includes.php';

if(!isget('Year')) {
	echo 'configTable.php did not receive a year';
	exit();
}

$Year = $_GET['Year'];

if(!preg_match('/([0-9]{4})/',$Year)) {
	echo __FILE__.' did not receive a valid year: '.$Year;
	exit();
}

$tableType = (isset($_GET['tableType']))?$_GET['tableType']:'data';

$file = $ROOT.'/'.$Year.'/tabledef_'.$tableType;			// was just tabledef_data

if(!is_readable($file)){
	echo __FILE__.' could not read the requested table definition: '.$file;
	exit();
}

$tbl = parseJSONfile($file);
$tableName = $tbl->tableName;
$cols = $tbl->columns;



if(!isset($_GET['included'])) {
// File is a standalone HTML document
	print html_top();
	print html_usual(true);
	print html_mid();
} else {
// File is being included
	print js_meta();
}

?>
<?php /************************/ ?>
<script>
// https://stackoverflow.com/questions/12602443/converting-table-data-into-json
(function($) {
    $.extend({
        toDictionary: function(query) {
            var parms = {};
            var items = query.split("&"); // split
            for (var i = 0; i < items.length; i++) {
                var values = items[i].split("=");
                var key = decodeURIComponent(values.shift());
                var value = values.join("=")
                parms[key] = decodeURIComponent(value);
            }
            return (parms);
        }
    })
})(jQuery);

    
(function($) {
    $.fn.serializeFormJSON = function() {
        var o = [];
        $(this).find('tr').each(function() {
            var elements = $(this).find('input, textarea, select')
//            console.log($(this));
            if (elements.length > 0) {
                var serialized = $(this).find('input, textarea, select').serialize();
                var item = $.toDictionary(serialized );
                o.push(item);
            }
        });
        return o;
    };
})(jQuery);
</script>


<script>
function tableToJSON() {
	var $j = $('#formtable').serializeFormJSON();
	for(let $r of $j) {
		if($r.type.search('int')!=-1) $r.basetype = 'i';
		if($r.type.search('char')!=-1) $r.basetype = 's';
		if($r.basetype == 's' && parseInt($r.type.replace(/[^0-9]/g,'')) > 80) $r.basetype = 'b';
	}
	return $j;
}

/*/
function genTableConfig() {
	if($('#EventName').val()) $EventName = $('#EventName').val();
	if($('#EventKey').val()) $EventKey = $('#EventKey').val();
	$tableType = "data";
	$tableName = $tableType+$Year+$EventKey;
	return {"tableName":$tableName,"tableType":$tableType,"Year":$Year,"EventKey":$EventKey,"FQEK":$Year+$EventKey,"SeasonSegment":"","EventName":$EventName,"columns":tableToJSON()};
}
/*/
function refreshEventValues() {
	if($('#EventName').val()) $EventName = $('#EventName').val();
	if($('#EventKey' ).val()) $EventKey  = $('#EventKey' ).val();
	$FQEK = $Year + $EventKey;
	$tableName = "data"+$FQEK;
}
function genTableConfig() {
	refreshEventValues();
	$tableType = "data";
	$tableName = $tableType+$FQEK;
	return {"tableName":$tableName,"tableType":$tableType,"Year":$Year,"EventKey":$EventKey,"FQEK":$FQEK,"EventName":$EventName,"SeasonSegment":"","columns":tableToJSON()};
}
/**/

function deleteRow($tr) {
	$($tr).parentsUntil('tbody').remove();
}

$(function(){
	$('[name=basetype]').parentsUntil('tr').remove();
	$('[name=extra]').toggle();
	$('#addRow').click(function() {
		$('#formtable').find('tr:last').after($('#formtable').find('tr:last').clone());
	});

	$('form.configTable').submit(function(event) {
		event.preventDefault();
		$j = genTableConfig();
		$j = JSON.stringify($j,null,1);
		$.when($.ajax({url:"/api/v2018/writeTableConfig.php",method:"POST",data:"tableType=<?=$tableType?>&data="+$j})).done(function($a){console.log($a);}); // added table=tabletype
	});
	$('legend').on('click',function(){$('#formtable').toggle();}); // allow hiding table
	$('#formtable').toggle();
});
</script>

<div id="Dir" style="display: none;"><?=(isget('EventKey'))?'&eventKey='.get('EventKey'):'';?></div>
<form class="configTable form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Editing Table:
<?=$tbl->tableType?><?=$Year?>
</legend>
<table id="formtable">
<tbody>

<?php
// From ./tabledefs.php

// TODO: Move to util.forms.php or something
function attr($attr,$text) {return ($text != '')?' '.$attr.'="'.$text.'"':$text;}
function input($name,$type='button',$value='',$placeholder='',$req='',$id='',$class='',$other='') {
return '<input '.attr('name',$name).attr('type',$type).attr('value',$value).attr('placeholder',$placeholder).attr('required',$req).attr('id',$id).attr('class',$class).$other.'>'.PHP_EOL;
}
////

foreach($cols as $n => $a) {
	echo '<tr>';
	foreach($a as $k => $v) {
		echo '<td>'.input($k,'text',$v,$v,false,null,'form-control input-md').'</td>';
	}
	echo '<td>'.input('remove','button','-',null,false,null,'btn btn-danger','onclick="deleteRow(this)"').'</td>';
	echo '</tr>';
}
?>

<?php //	     Name	Type		Value		Placeholder	Req.	id	class			other ?>
<!--
<tr>
<td><?=input('name',		'text',	'teamNumber',	'teamNumber',	false,	'name')?></td>
<td><?=input('type',		'text',	'tinyint',	'tinyint',	false,	'type')?></td>
<td><?=input('title',	'text',	'Team Number','Team Number',false,	'title')?></td>
<td><?=input('remove',	'button',	'-',		null,		false,	null,	'btn btn-danger',	'onclick="deleteRow(this)"')?></td>
</tr>
-->

</tbody>
</table>
<button id="addRow" class="btn btn-success" type="button">+</button>
<button id="saveConfig" class="btn btn-success" type="submit">Save Configuration</button>
</fieldset>
</form>

<?php /************************/ ?>

<?php

if(!isset($_GET['included'])) {
	print html_bottom();
}

?>