<?php
// Define classes for Season, Event, Match

class Season implements JsonSerializable{
	public $name = 'Uninitialized Season Name';
	public $year = '2000';
	public $events = array();
	public $currentEvent = '';
	
	public function expose() {return get_object_vars($this);}
	public function jsonSerialize() {return [
		'name' => $this->name,
		'year' => $this->year,
		'events' => $this->events,
		'currentEvent' => $this->currentEvent
	];}
}

class Event implements JsonSerializable{
	public $name = 'Uninitialized Event Name';
	public $key = 'uen';
	public $year = '2000';
	public function fqek() {return $this->year.$this->key;}
	public $matches = array();
	public $currentMatch = ''
	
	public function expose() {return get_object_vars($this);}
	public function jsonSerialize() {return [
		'name' => $this->name,
		'key' => $this->key,
		'year' => $this->year,
		'fqek' => $this->fqek(),
		'matches' => $this->matches,
		'currentMatch' => $this->currentMatch
	];}
}

class Match implements JsonSerializable{
	public $event_key = '';
	public $comp_level = ''; // choose from q, ef, qf, sf, f
	public $set_number = ''; // either '' (if q) or a natural number (otherwise)
	public $match_number = 0;
	public function key() {return $this->event_key.'_'.$this->comp_level.$this->set_number.$this->match_number;}
	public alliances = array();
	public function expose() {return get_object_vars($this);}
	public function jsonSerialize() {return [
		'event_key' => $this->event_key,
		'comp_level' => $this->comp_level,
		'set_number' => $this->set_number,
		'match_number' => $this->match_number,
		'key' => $this->key()
	];}
}

class Alliance implements JsonSerializable{
	public $teams = array();
	
	public function expose() {return get_object_vars($this);}
	public function jsonSerialize() {return $teams;}
}

?>