<?php
ini_set('short_open_tag', 1);
if(!isset($ROOT_UTIL_INCLUDED)){
$ROOT = dirname(__FILE__);
function val($foo) {if($foo==0 || $foo=="0"){return "";}else{return $foo;}}
function bool($foo) {if($foo=="" || $foo == 1 || $foo == "1"){return "Y";}else{return "N";}}
function p($f) {return '<pre>'.$f.'</pre>';} // DEPRECATED 2018 use pre()
function pre($f) {return '<pre>'.$f.'</pre>';}
function e($f) {return $f.'<br/>';}
function check($value) {return (isset($_GET[$value]))?' and '.$value.'="'.$_GET[$value].'"':'';}
function get($foo) {return (isset($_GET[$foo]))?$_GET[$foo]:'';}
function post($foo) {return (isset($_POST[$foo]))?$_POST[$foo]:'';}
function isget($foo) {return (isset($_GET[$foo]))?true:false;}
function ispost($foo) {return (isset($_POST[$foo]))?true:false;}
function vowelify($str) {return preg_replace('([aeiouAEIOU])','<a href="..">$0</a>',$str);}

//function hasClass($classes, $class) {if(isset($classes->class)) $classes=$classes->class; return strpos($classes, $class)!==False;}
function hasClass($classes, $class) {if(isset($classes->class)) $classes=$classes->class; return preg_match('/\\b'.$class.'\\b/',$classes);}

function array_multidim_unique($array, $key) { 
    $temp_array = array(); 
    $i = 0; 
    $key_array = array(); 
    
    foreach($array as $val) { 
        if (!in_array($val[$key], $key_array)) { 
            $key_array[$i] = $val[$key]; 
            $temp_array[$i] = $val; 
        } 
        $i++; 
    } 
    return $temp_array; 
}

function getIndex($value,$low,$high){
	$top = $high - $low;
	$value = $value - $low;
	$scaled = $value / $top;
	return $scaled;
}
function scale($value,$low,$high){
	if($low>$high){return scale($low-$value,0,$low-$high);} else {
		if($low==$high && $value==$high) return 0.5;
		if($value>$high) return 1;
		if($value<$low) return 0;
		return (($value - $low) / ($high - $low));
	}
}
function dynColor($u,$a,$b){
	$scaled = scale($u,$a,$b);
	return 'rgb('
		.round(255*(1-$scaled))
		.','
		.round(255*$scaled)
		.',0)';
}
function quickColor($u,$a,$b){
	$style = ' style="background-color: '.dynColor($u,$a,$b).';" ';
	return $style;
}
function styleIfNotZero($u,$a,$b){return ($u!=0)?quickColor($u,$a,$b):'';}

function css($file){
	global $Year;
	$file .= '.css';
	$d = dirname(__FILE__);
	$css = '';
	if(file_exists($d.'/css/'.$file)) $css .= html_link('text/css','stylesheet','/css/'.$file.'?'.time());
	if(file_exists($d.'/css/'.$Year.'/'.$file)) $css .= html_link('text/css','stylesheet','/css/'.$Year.'/'.$file.'?'.time());
	if($d != dirname(dirname($_SERVER["SCRIPT_FILENAME"])) && file_exists('../css/'.$file)) $css .= html_link('text/css','stylesheet','../css/'.$file.'?'.time());
	if(file_exists('css/'.$file)) $css .= html_link('text/css','stylesheet','css/'.$file.'?'.time());
	if(file_exists($file)) $css .= html_link('text/css','stylesheet',$file.'?'.time());
	return $css;
}

function js($file){
	global $Year;
	$subdir = (file_exists(dirname(__FILE__).'/js/'.$Year.'/'.$file.'.js'))?$Year.'/':'';
	return (file_exists(dirname(__FILE__).'/js/'.$subdir.$file.'.js'))?'<script src="/js/'.$subdir.$file.'.js?'.time().'"></script>'.PHP_EOL:'';
}

function jsinclude($file){
	global $Year;
	print '<script>'.PHP_EOL;
	$subdir = (file_exists(dirname(__FILE__).'/js/'.$Year.'/'.$file.'.js'))?$Year.'/':'';
	include dirname(__FILE__).'/js/'.$subdir.$file.'.js';
	print PHP_EOL.'</script>'.PHP_EOL;
	return '';
}

/** $element is an array containing attr => value pairs
 *  $attrs is an array containing a list of which attributes to collect
 */
function attribute_string($element, $attrs) {
	$str = '';
	foreach($attrs as $attr)
	{
		$str .= ($element[$attr]=='')?'':' '.$attr.'="'.$element[$attr].'"';
	}
	return $str;
}

function button($tgt,$name){return '<a class="button" href="'.$tgt.'">'.$name.'</a>';} // DEPRECATED 2018 dnu

function parseJSON($string){
	return json_decode($string,substr($string,0,1)=='[');
}

function curlJSON($url,$is_array=false){
	// create a new cURL resource
	$ch = curl_init();
	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// grab URL and pass it to the browser
	$json = curl_exec($ch);
	// close cURL resource, and free up system resources
	curl_close($ch);
	$d = ($is_array)?json_decode($json,true):json_decode($json);
	return $d;
}
function curl($url){
	// create a new cURL resource
	$ch = curl_init();
	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// grab URL and pass it to the browser
	$data = curl_exec($ch);
	// close cURL resource, and free up system resources
	curl_close($ch);
	return $data;
}

function guid() {
//    if (function_exists('com_create_guid') === true) { return trim(com_create_guid(), '{}'); }
    return sprintf('{%04X%04X-%04X-%04X-%04X-%04X%04X%04X}', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}

$nl = '<br/>';

// http://php.net/manual/en/function.copy.php
function recurse_copy($src,$dst) { 
    $dir = opendir($src); 
    @mkdir($dst); 
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                recurse_copy($src . '/' . $file,$dst . '/' . $file); 
            } 
            else { 
                copy($src . '/' . $file,$dst . '/' . $file); 
            } 
        } 
    } 
    closedir($dir); 
} 

//include dirname(__FILE__).'/parse.php';

function parseJSONfile($file) {
	if(is_readable($file)) {
		$json = file_get_contents($file);
		$array = (substr(trim($json),0,1)=='[')?true:false;
		return json_decode($json,$array);
	} else return false;
}

function js_meta() {
	$defined_vars = $GLOBALS;
	ksort($defined_vars);
	$v = '<script>'.PHP_EOL;
	foreach($defined_vars as $key => $value) if(!($key=='password' || $key=='username' || $key=='defined_vars') && !is_array($defined_vars[$key]) && !is_object($defined_vars[$key]))
		$v .= 'var $'.$key.' = (isNaN("'.$value.'"-0))?"'.$value.'":"'.$value.'"-0;'.PHP_EOL;
	$v .= '</script>'.PHP_EOL;
	return $v;
}

function favicon() {
	return '<link rel="icon" href="http://team1323.com/wp-content/uploads/2017/07/cropped-coyote-blue-transparent-512-32x32.png" sizes="32x32">'.PHP_EOL.
		'<link rel="icon" href="http://team1323.com/wp-content/uploads/2017/07/cropped-coyote-blue-transparent-512-192x192.png" sizes="192x192">'.PHP_EOL.
		'<link rel="apple-touch-icon-precomposed" href="http://team1323.com/wp-content/uploads/2017/07/cropped-coyote-blue-transparent-512-180x180.png">'.PHP_EOL;
}

function html_meta($name, $content){return '<meta name="'.$name.'" content="'.$content.'">'.PHP_EOL;}
function html_link($type,$rel,$href){return '<link type="'.$type.'" rel="'.$rel.'" href="'.$href.'">'.PHP_EOL;}
function html_title($title){return '<title>'.$title.'</title>'.PHP_EOL;}
//function jquery(){return  '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>'.PHP_EOL;} // not sure if this is a good idea
function jquery(){return '<script src="/js/jquery.min.js"></script>';} // ??

function html_top($id = '') {$id = ($id=='')?'':' id="'.$id.'"'; return '<!doctype html><html'.$id.'><head>';}
function html_usual_meta() {
	$meta = '';
	$meta .= '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">'.PHP_EOL;
	$meta .= '<meta http-equiv="Cache-Control" content="No-Cache">'.PHP_EOL;
	$meta .= html_meta('apple-mobile-web-app-capable','yes');
	$meta .= html_meta('viewport','width=device-width, initial-scale=1.0');
	$meta .= html_meta('expires','0');
	return $meta;
}
function html_usual($bootstrap = false) {
	$stuff = html_usual_meta();
	$stuff .= favicon();
	$stuff .= js('jquery.min')
		.js('jquery.tablesorter')
		.js('jquery.metadata')
		.js('jquery.units')
		.js('links-local')
		.js_meta();
	$stuff .= js(basename($_SERVER['SCRIPT_NAME'],'.php'));
	if($bootstrap) $stuff .= css('bootstrap').js('bootstrap');
	$stuff .= css(basename($_SERVER['SCRIPT_NAME'],'.php'));
	return $stuff;
}
function html_mid() {return '</head>'.PHP_EOL.'<body>';}
function html_bottom() {return '</body>'.PHP_EOL.'</html>';}

$ROOT_UTIL_INCLUDED = true;
}
?>

<?php
if(isset($SIGMA) && $SIGMA==false){
//	js('sigma/sigma.min');
	$graph = '';
	$lastNode = '';
	$lastX = 0;
	function InitGraph($graphName,$container){global $graph;
		$graph = $graphName;
		return 'var '.$graph.' = new sigma("'.$container.'");
		';}
	function LonelyNode($id,$label,$x,$y,$size,$color){global $graph;
		return $graph.'.graph.addNode({id: "'.$id.'", label: "'.$label.'", x: '.$x.', y: '.$y.', size: '.$size.', color: "'.$color.'"});
		';
	}
	function InitNode($id,$label,$x,$y,$size,$color){global $graph,$lastNode;
		$lastNode = $id;
		return LonelyNode($id,$label,$x,$y,$size,$color);
	}
	function Edge($from,$to){global $graph;
		return $graph.'.graph.addEdge({id: "'.$from.'_'.$to.'", source: "'.$from.'", target: "'.$to.'"});
		';
	}
	function TraceNode($id,$label,$x,$y,$size,$color,$lastNode){global $graph;
		$newNode = LonelyNode($id,$label,$x,$y,$size,$color);
		$newEdge = ($lastNode=='')?'':Edge($lastNode,$id);
		return $newNode.$newEdge;
	}
	function AppendNode($id,$label,$x,$y,$size,$color){global $graph,$lastNode;
		$newNode = LonelyNode($id,$label,$x,$y,$size,$color);
		$newEdge = ($lastNode=='')?'':Edge($lastNode,$id);
		$lastNode = $id;
		return $newNode.$newEdge;
	}
	function NextDatum($label,$y){global $graph,$lastX;
		$size = 1; $color = '#000';
		return ($lastX==0)?InitNode('x'.$lastX,$label,$lastX++,$y,$size,$color):AppendNode('x'.$lastX,$label,$lastX++,$y,$size,$color);
	}
	function Refresh(){global $graph;
		return $graph.'.refresh();
		';
	}
	$SIGMA = true;
}
?>

<?php
if(isset($CHART) && $CHART==false){
	$graph = '';
	$lastNode = '';
	$lastX = 0;
	function InitGraph($graphName,$container){global $graph;
	}
	function LonelyNode($id,$label,$x,$y,$size,$color){global $graph;
	}
	function InitNode($id,$label,$x,$y,$size,$color){global $graph,$lastNode;
	}
	function Edge($from,$to){global $graph;
	}
	function TraceNode($id,$label,$x,$y,$size,$color,$lastNode){global $graph;
	}
	function AppendNode($id,$label,$x,$y,$size,$color){global $graph,$lastNode;
	}
	function NextDatum($label,$y){global $graph,$lastX;
	}
	function Refresh(){global $graph;
	}
	$CHART = true;
}
?>

<?php
if(isset($CHARTIST) && $CHARTIST==false){
	$graph = '';
	$lastNode = '';
	$lastX = 0;
	function InitGraph($graphName,$container){global $graph;
	}
	function LonelyNode($id,$label,$x,$y,$size,$color){global $graph;
	}
	function InitNode($id,$label,$x,$y,$size,$color){global $graph,$lastNode;
	}
	function Edge($from,$to){global $graph;
	}
	function TraceNode($id,$label,$x,$y,$size,$color,$lastNode){global $graph;
	}
	function AppendNode($id,$label,$x,$y,$size,$color){global $graph,$lastNode;
	}
	function NextDatum($label,$y){global $graph,$lastX;
	}
	function Refresh(){global $graph;
	}
	$CHARTIST = true;
}
?>